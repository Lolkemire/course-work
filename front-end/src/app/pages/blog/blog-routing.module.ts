import {RouterModule, Routes} from '@angular/router';
import {BlogComponent} from './blog.component';
import {NgModule} from '@angular/core';
import {ArticleListComponent} from './article-list/article-list.component';
import {ArticlesResolver} from './article-list/resolvers/articles.resolver';
import {AuthenticatedOnlyGuard} from '../../shared/guards/authenticated-only.guard';
import {NotAdminGuard} from '../../shared/guards/not-admin.guard';
import {CreateArticleComponent} from './create-article/create-article.component';
import {UpdateArticleComponent} from './update-article/update-article.component';
import {ArticleResolver} from './resolvers/article.resolver';
import {ArticleOwnerOrAdminGuard} from './guards/article-owner-or-admin.guard';

const routes: Routes = [
  {
    path: '',
    component: BlogComponent,
    children: [
      {
        path: 'create',
        canActivate: [
          AuthenticatedOnlyGuard, NotAdminGuard,
        ],
        component: CreateArticleComponent,
      },
      {
        path: 'update/:id',
        resolve: {
          article: ArticleResolver
        },
        canActivate: [
          AuthenticatedOnlyGuard,
          // ArticleOwnerOrAdminGuard,
        ],
        component: UpdateArticleComponent,
      },
      {
        path: 'discover',
        resolve: {
          articles: ArticlesResolver
        },
        component: ArticleListComponent,
      },
      {
        path: '**',
        redirectTo: 'discover',
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BlogRoutingModule {

}
