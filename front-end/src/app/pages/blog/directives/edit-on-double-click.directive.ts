import {Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[editOnDoubleClick]'
})
export class EditOnDoubleClickDirective {
  @Input() public initialState;
  @Output() public state = new EventEmitter<boolean>();
  @Output() public editEnded = new EventEmitter<{old: string, new: string}>();
  public originalText: string;

  constructor(private el: ElementRef) {
    this.el.nativeElement.readOnly = this.initialState || true;
    this.originalText = this.currentText;
  }

  @HostListener('dblclick') onDoubleClick() {
    this.changeState();
  }

  @HostListener('keydown', ['$event']) onKeydown(event: KeyboardEvent) {
    this.onEnterClicked(event);
    this.onEscapeClicked(event);
  }

  onEscapeClicked(event: KeyboardEvent) {
    const escapeCode = 27;
    if (event.keyCode === escapeCode && !this.readOnly) {
      this.currentText = this.originalText;
      this.changeState();
    }
  }

  onEnterClicked(event: KeyboardEvent) {
    const isEnterClicked = event.keyCode === 13;
    const isEnterClickedAnotherCode = event.keyCode === 10;
    if (this.readOnly) {
      return;
    }
    if (!event.ctrlKey && (isEnterClicked || isEnterClickedAnotherCode)) {
      event.preventDefault();
      this.changeState();
    }
    if (event.ctrlKey && (isEnterClicked || isEnterClickedAnotherCode)) {
      this.currentText += '\n';
    }
  }

  changeState() {
    this.el.nativeElement.readOnly = !this.el.nativeElement.readOnly;
    this.onEdit();
  }

  onEdit() {
    this.state.emit(this.readOnly);
    if (!this.readOnly) {
      this.originalText = this.currentText;
    } else if (this.originalText !== this.currentText) {
      this.editEnded.emit({
        old: this.originalText,
        new: this.currentText,
      });
    }
  }

  get currentText() {
    return this.el.nativeElement.value;
  }

  set currentText(value: string) {
    this.el.nativeElement.value = value;
  }

  get readOnly() {
    return this.el.nativeElement.readOnly;
  }
}
