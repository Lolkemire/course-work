import {AfterViewInit, Directive, ElementRef, HostListener, OnInit} from '@angular/core';

@Directive({
  selector: '[autoExpand]'
})
export class AutoExpandDirective implements AfterViewInit {
  constructor(private el: ElementRef) {
    console.log(el);
  }

  ngAfterViewInit(): void {
    this.resize();
  }

  @HostListener('keydown')
  @HostListener('input')
  onKeyDown() {
    this.resize();
  }

  resize() {
    this.el.nativeElement.style.height = 'auto';
    const scrollHeight = this.el.nativeElement.scrollHeight;
    this.el.nativeElement.style.height = `${scrollHeight}px`;
  }

}
