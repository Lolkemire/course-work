import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BlogComponent} from './blog.component';
import {ArticlePopupModalComponent} from './article-popup-modal/article-popup-modal.component';
import {BlogRoutingModule} from './blog-routing.module';
import {ArticleListComponent} from './article-list/article-list.component';
import {ArticlesResolver} from './article-list/resolvers/articles.resolver';
import {ArticlesService} from './services/articles.service';
import {CommentsService} from './services/comments.service';
import {ArticleListItemComponent} from './article-list/article-list-item/article-list-item.component';
import {CollapseModule, ModalModule} from 'ngx-bootstrap';
import {CommentsListComponent} from './components/comments-list/comments-list.component';
import {CommentsListItemComponent} from './components/comments-list/comments-list-item/comments-list-item.component';
import {CreateCommentComponent} from './components/create-comment/create-comment.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ArticleFormComponent} from './components/article-form/article-form.component';
import {FileUploadModule} from 'ng2-file-upload';
import {CreateArticleComponent} from './create-article/create-article.component';
import {UpdateArticleComponent} from './update-article/update-article.component';
import {ArticleResolver} from './resolvers/article.resolver';
import {ArticleOwnerOrAdminGuard} from './guards/article-owner-or-admin.guard';
import {EditOnDoubleClickDirective} from './directives/edit-on-double-click.directive';
import {ImagePreview} from '../../image-preview/directives/imagepreview.directive';
import { AutoExpandDirective } from './directives/auto-expand.directive';
import {ImagePreviewModule} from '../../image-preview/image-preview.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    BlogRoutingModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    ImagePreviewModule,
  ],
  declarations: [
    BlogComponent,
    ArticlePopupModalComponent,
    ArticleListComponent,
    ArticleListItemComponent,
    CommentsListComponent,
    CommentsListItemComponent,
    CreateArticleComponent,
    CreateCommentComponent,
    ArticleFormComponent,
    UpdateArticleComponent,
    EditOnDoubleClickDirective,
    AutoExpandDirective,
  ],
  providers: [
    ArticlesResolver,
    ArticleResolver,

    ArticleOwnerOrAdminGuard,

    ArticlesService,
    CommentsService,
  ],
  entryComponents: [
    ArticlePopupModalComponent
  ]
})
export class BlogModule { }
