import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {SessionService} from "../../../shared/services/session.service";
import {Article} from "../models/article";

@Injectable()
export class ArticleOwnerOrAdminGuard implements CanActivate {
  constructor(private sessionService: SessionService, private router: Router) {

  }

  // DOES NOT WORKS

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const article = next.data['article'];
    return this.isUserArticleAuthor(article);
  }

  isUserArticleAuthor(article: Article) {
    return new Observable<boolean>(observer => {
      this.sessionService.currentUser.subscribe(user => {
        if ((article && user) && (article.author_id === user.id)) {
          observer.next(true);
        } else {
          this.router.navigate(['/']);
          observer.next(false);
        }
      });
    });
  }


}
