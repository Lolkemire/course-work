import { TestBed, async, inject } from '@angular/core/testing';

import { ArticleOwnerOrAdminGuard } from './article-owner-or-admin.guard';

describe('ArticleOwnerOrAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArticleOwnerOrAdminGuard]
    });
  });

  it('should ...', inject([ArticleOwnerOrAdminGuard], (guard: ArticleOwnerOrAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
