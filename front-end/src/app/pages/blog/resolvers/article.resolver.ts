import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article';


@Injectable()
export class ArticleResolver implements Resolve<Article> {
  constructor(private articlesService: ArticlesService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Article> | Promise<Article> | Article {
    return this.articlesService.get(route.params['id']);
  }
}
