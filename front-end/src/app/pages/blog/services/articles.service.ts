import { Injectable } from '@angular/core';
import ApiService from '../../../shared/services/api.service';
import {Http} from '@angular/http';
import {SessionService} from '../../../shared/services/session.service';
import {plainToClass} from 'class-transformer';
import {Article} from '../models/article';
import {Comment} from '../models/comment';

@Injectable()
export class ArticlesService extends ApiService {
  protected apiPath = '/api/v1/articles';

  constructor(protected http: Http, protected sessionService: SessionService) {
    super(sessionService);
  }

  /**
   * Return all articles
   * @returns {Observable<Article[]>}
   */
  public getAll() {
    return this.mapRequest(this.http.get(this.apiPath, {headers: this.getHeaders()}))
      .map(res => plainToClass(Article, res as Object[]));
  }

  /**
   * Return article by assigned id
   * @param {number} id
   * @returns {Observable<Article[]>}
   */
  public get(id: number) {
    const path = `${this.apiPath}/${id}`;
    return this.mapRequest(this.http.get(path, {headers: this.getHeaders()}))
      .map(res => plainToClass(Article, res));
  }

  /**
   * Perform request for article creation
   * @param {FormData} formData
   * @returns {Observable<Article[]>}
   */
  public create(formData: FormData) {
    const headers = this.getMultiPartHeaders();
    return this.mapRequest(this.http.post(this.apiPath, formData, {headers: headers}))
      .map(res => plainToClass(Article, res));
  }

  /**
   * Perform request for article update
   * @param {Article} article
   * @param {FormData} formData
   * @returns {Observable<Article[]>}
   */
  public update(article: Article, formData: FormData) {
    const headers = this.getMultiPartHeaders();
    const path = `${this.apiPath}/${article.id}`;
    return this.mapRequest(this.http.patch(path, formData, {headers: headers}))
      .map(res => plainToClass(Article, res));
  }

  /**
   * Upload articles comments
   * @param {number} articleId
   * @returns {Observable<Comment[]>}
   */
  public uploadComments(articleId: number) {
    const path = `${this.apiPath}/${articleId}/comments`;
    return this.mapRequest(this.http.get(path, {headers: this.getHeaders()}))
      .map(res => plainToClass(Comment, res as Object[]));
  }

  /**
   * Destroy article
   * @param {Article} article
   * @returns {Observable<any | any>}
   */
  public destroy(article: Article) {
    const path = `${this.apiPath}/${article.id}`;
    return this.mapRequest(this.http.delete(path, {headers: this.getHeaders()}));
  }

  protected getMultiPartHeaders() {
    const headers = this.getHeaders();
    headers.append('enctype', 'multipart/form-data');
    headers.append('Cache-Control', 'no-cache');
    headers.append('Cache-Control', 'no-store');
    headers.append('Pragma', 'no-cache');
    return headers;
  }
}
