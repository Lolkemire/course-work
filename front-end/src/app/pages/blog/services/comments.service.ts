import { Injectable } from '@angular/core';
import ApiService from "../../../shared/services/api.service";
import {Http} from "@angular/http";
import {SessionService} from "../../../shared/services/session.service";
import {plainToClass} from "class-transformer";
import {Comment} from "../models/comment";

@Injectable()
export class CommentsService extends ApiService {
  apiPath = '/api/v1/comments';

  constructor(private http: Http, protected sessionService: SessionService) {
    super(sessionService);
  }

  public create(data: any) {
    return this.mapRequest(this.http.post(this.apiPath, data, {headers: this.getHeaders()}))
      .map(res => plainToClass(Comment, res));
  }

  public update(comment: Comment, dataToUpdate: any) {
    const path = `${this.apiPath}/${comment.id}`;
    return this.mapRequest(this.http.patch(path, dataToUpdate, {headers: this.getHeaders()}))
      .map(res => plainToClass(Comment, res));
  }

  public delete(comment: Comment) {
    const path = `${this.apiPath}/${comment.id}`;
    return this.mapRequest(this.http.delete(path, {headers: this.getHeaders()}));
  }

}
