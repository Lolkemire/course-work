import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {ArticlesService} from '../../services/articles.service';
import {User} from '../../../../shared/models/user';
import {SessionService} from '../../../../shared/services/session.service';
import {Article} from '../../models/article';


@Injectable()
export class ArticlesResolver implements Resolve<Article[]> {
  constructor(private sessionService: SessionService, private articlesService: ArticlesService)  { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return this.articlesService.getAll();
  }
}
