import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Article} from '../models/article';
import {BsModalRef, BsModalService, ModalDirective} from 'ngx-bootstrap';
import {ArticlePopupModalComponent} from '../article-popup-modal/article-popup-modal.component';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ArticleListComponent implements OnInit {
  public articles: Article[];
  bsModalRef: BsModalRef;

  constructor(private route: ActivatedRoute, private modalService: BsModalService,
              private titleService: Title) {
    this.titleService.setTitle('Discover');
  }

  ngOnInit() {
    this.route.data.subscribe(val => {
      this.articles = val['articles'];
    });
  }

  openModal(article: Article) {
    this.modalService.config.class = 'article-modal';
    this.modalService.config.keyboard = false;
    this.bsModalRef = this.modalService.show(ArticlePopupModalComponent);
    this.bsModalRef.content.articles = this.articles;
    this.bsModalRef.content.currentArticle = article;
    this.bsModalRef.content.onArticleDelete.subscribe(this.onArticleDeleted.bind(this));
  }

  onArticleDeleted(deletedArticle: Article) {
    const index = this.articles.indexOf(deletedArticle);
    delete this.articles[index];
  }
}
