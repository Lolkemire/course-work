import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FileUploader} from 'ng2-file-upload';
import {ArticlesService} from '../services/articles.service';
import {Article} from '../models/article';
import ApiError from '../../../shared/models/api-error';
import {Router} from '@angular/router';
import {AlertsService} from '../../../shared/services/alerts.service';
import {AlertTypes} from '../../../shared/models/alert-types.enum';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss']
})
export class CreateArticleComponent implements OnInit {

  constructor(private articlesService: ArticlesService, private router: Router, private alertsService: AlertsService) {
  }

  ngOnInit() {
  }


  onSubmit(formData: FormData) {
    this.articlesService.create(formData)
      .subscribe(this.onSuccess.bind(this), this.onFail.bind(this));
  }

  onSuccess(article: Article) {
    const msg = `Article ${article.title} was create!`;
    this.alertsService.push({
      type: AlertTypes.PRIMARY,
      msg: msg,
      timeout: 5000,
      dismissable: true,
    });
    this.router.navigate(['/blog']);
  }

  onFail(err: ApiError) {
    const message = (err.message.detail) ? err.message.detail : 'Article wasn\'t create!';
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: message,
      timeout: 5000,
      dismissable: true,
    });
  }


}
