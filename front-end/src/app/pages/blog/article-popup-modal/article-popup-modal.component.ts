import {Component, EventEmitter, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Article} from '../models/article';
import {BsModalRef} from 'ngx-bootstrap';
import {CommentsService} from '../services/comments.service';
import {ArticlesService} from '../services/articles.service';
import {AlertsService} from '../../../shared/services/alerts.service';
import {AlertTypes} from '../../../shared/models/alert-types.enum';
import {SessionService} from '../../../shared/services/session.service';
import {User} from '../../../shared/models/user';
import ApiError from '../../../shared/models/api-error';
import {Router} from '@angular/router';
import {Comment} from '../models/comment';

@Component({
  selector: 'modal-content',
  templateUrl: './article-popup-modal.component.html',
  styleUrls: ['./article-popup-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ArticlePopupModalComponent implements OnInit {
  public currentArticle: Article;
  public articles: Article[];
  public isCommentsCollapsed = true;
  public comments: Comment[];
  public currentUser: User;
  public isAuthenticated: boolean;
  public onArticleDelete = new EventEmitter<Article>();

  private prevArticle: Article;
  private nextArticle: Article;

  constructor(public bsModalRef: BsModalRef, private commentsService: CommentsService,
              private articlesService: ArticlesService, private alertsService: AlertsService,
              private router: Router,
              private sessionService: SessionService) {
    this.sessionService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.isAuthenticated = this.sessionService.authenticated;
  }

  /**
   * Handle click on prev article button
   */
  onPrevClicked() {
    this.collapseCommentSection();
    this.currentArticle = this.prevArticle;
  }

  /**
   * Handle click on next article button
   */
  onNextClicked() {
    this.collapseCommentSection();
    this.currentArticle = this.nextArticle;
  }

  /**
   * Collapse comments section and clear stored comments
   */
  collapseCommentSection() {
    this.isCommentsCollapsed = true;
    this.comments = [];
  }

  /**
   * Upload article comments on comments section expand
   */
  onExpand() {
    if (!this.isCommentsCollapsed) {
      this.articlesService.uploadComments(this.currentArticle.id)
        .subscribe(this.onCommentsUploaded.bind(this), this.onCommentsUploadFail.bind(this));
    }
  }

  /**
   * Store recieved comments
   * @param {Comment[]} comments
   */
  onCommentsUploaded(comments: Comment[]) {
    this.comments = comments;
  }

  /**
   * Push error alert
   * @param {ApiError} err
   */
  onCommentsUploadFail(err: ApiError) {
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: 'Comment wasn\' loaded!',
      timeout: 2000,
      dismissable: true,
    });
  }

  /**
   * Store new comment to array, push notification
   * @param {Comment} comment
   */
  onCommentCreated(comment: Comment) {
    this.comments.unshift(comment);
    this.alertsService.push({
      type: AlertTypes.PRIMARY,
      msg: 'Commentary was successful create!',
      timeout: 5000,
      dismissable: true,
    });
  }

  /**
   * Perform article deletion
   */
  onDelete() {
    this.articlesService.destroy(this.currentArticle)
      .subscribe(this.onDeleteSuccess.bind(this), this.onDeleteFail.bind(this));
  }

  /**
   * Emit on delete event
   * Push notification
   * Hide modal
   */
  onDeleteSuccess() {
    this.onArticleDelete.emit(this.currentArticle);
    this.alertsService.push({
      type: AlertTypes.PRIMARY,
      msg: 'Article was deleted!',
      timeout: 5000,
      dismissable: true,
    });
    this.bsModalRef.hide();
  }

  /**
   * Push error notification
   * @param {ApiError} err
   */
  onDeleteFail(err: ApiError) {
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: 'Article wasn\'t deleted!',
      timeout: 5000,
      dismissable: true,
    });
  }

  /**
   * Perform redirection on update article page
   */
  onUpdateClick() {
    this.router.navigate(['/blog', 'update', this.currentArticle.id.toString()]);
    this.bsModalRef.hide();
  }

  get canDelete() {
    return this.currentUser && this.currentUser.isAdmin;
  }

  get canUpdate() {
    if (!this.currentUser) {
      return false;
    }
    const isAuthor = this.currentUser.id === this.currentArticle.author_id;
    const isAdmin = this.currentUser.isAdmin;
    return this.currentUser && (isAdmin || isAuthor);
  }

  /**
   * Check can user go to prev article
   * @returns {boolean}
   */
  get canPrev() {
    const currentArticleIndex = this.articles.indexOf(this.currentArticle);
    this.prevArticle = this.articles[currentArticleIndex - 1];
    return !!(this.prevArticle);
  }

  /**
   * Check can user go to next article
   * @returns {boolean}
   */
  get canNext() {
    const currentArticleIndex = this.articles.indexOf(this.currentArticle);
    this.nextArticle = this.articles[currentArticleIndex + 1];
    return !!(this.nextArticle);

  }
}
