import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlePopupModalComponent } from './article-popup-modal.component';

describe('ArticlePopupModalComponent', () => {
  let component: ArticlePopupModalComponent;
  let fixture: ComponentFixture<ArticlePopupModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlePopupModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlePopupModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
