import {User} from '../../../shared/models/user';
import {Type} from 'class-transformer';

export class Article {
  id: number;
  title: string;
  text: string;
  image: string;
  author_id: number;
  author: User;

  @Type(() => Date)
  createdAt: Date;

  @Type(() => Date)
  updatedAt: Date;

  get authorInfo() {
    return this.author.username;
  }

}
