import {User} from '../../../shared/models/user';
import {Type} from 'class-transformer';

export class Comment {
  id: number;
  author_id: number;
  author: User;
  article_id: number;
  text: string;

  @Type(() => Date)
  createdAt: Date;

  @Type(() => Date)
  updatedAt: Date;

  get isUpdated() {
    return this.createdAt !== this.updatedAt;
  }

  get createDate() {
    return (this.isUpdated) ? this.updatedAt.toLocaleString() : this.createdAt.toISOString();
  }
}
