import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticlesService} from '../services/articles.service';
import {AlertsService} from '../../../shared/services/alerts.service';
import {Article} from '../models/article';
import {AlertTypes} from '../../../shared/models/alert-types.enum';
import ApiError from '../../../shared/models/api-error';
import {SessionService} from '../../../shared/services/session.service';

@Component({
  selector: 'app-update-article',
  templateUrl: './update-article.component.html',
  styleUrls: ['./update-article.component.scss']
})
export class UpdateArticleComponent implements OnInit {
  public article: Article;

  constructor(private articlesService: ArticlesService,
              private router: Router,
              private route: ActivatedRoute,
              private sessionService: SessionService,
              private alertsService: AlertsService) {

  }

  ngOnInit() {
    this.route.data.subscribe(val => {
      this.article = val['article'];
    });
    this.checkHasPermissionForUpdate();
  }

  /**
   * Perform user permission check for article update
   */
  checkHasPermissionForUpdate() {
    // This must me a guard
    // But guard canActivate function invoke before
    // article resolver fetch article by id
    this.sessionService.currentUser.subscribe(user => {
      const isAuthor = user && this.article.author_id === user.id;
      const isAdmin = user && user.isAdmin;
      if ((user && this.article) && (!isAuthor && !isAdmin)) {
        this.router.navigate(['/']);
      }
    });
  }

  /**
   * Perfrom article update on submit
   * @param {FormData} formData
   */
  onSubmit(formData: FormData) {
    this.articlesService.update(this.article, formData)
      .subscribe(this.onSuccess.bind(this), this.onFail.bind(this));
  }

  /**
   * Push alert and redirect on success
   * @param {Article} article
   */
  onSuccess(article: Article) {
    const msg = `Article ${article.title} was updated!`;
    this.alertsService.push({
      type: AlertTypes.PRIMARY,
      msg: msg,
      timeout: 5000,
      dismissable: true,
    });
    this.router.navigate(['/blog']);
  }

  /**
   * Push error alert on fail
   * @param {ApiError} err
   */
  onFail(err: ApiError) {
    const message = (err.message.detail) ? err.message.detail : 'Article wasn\'t updated!';
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: message,
      timeout: 5000,
      dismissable: true,
    });
  }
}
