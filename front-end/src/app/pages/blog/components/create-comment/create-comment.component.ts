import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CommentsService} from '../../services/comments.service';
import {Comment} from '../../models/comment';
import ApiError from '../../../../shared/models/api-error';
import {Article} from '../../models/article';
import {User} from "../../../../shared/models/user";
import {SessionService} from "../../../../shared/services/session.service";

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.scss']
})
export class CreateCommentComponent implements OnInit {
  public createCommentForm: FormGroup;
  public currentUser: User;

  @Input() public article: Article;
  @Output() public commentaryCreated = new EventEmitter<Comment>();
  @Output() public creationError = new EventEmitter<ApiError>();

  constructor(private commentsService: CommentsService, private sessionService: SessionService) {
    this.createCommentForm = new FormGroup({
      'text': new FormControl('', [Validators.required,
        Validators.maxLength(255)])
    });
    this.sessionService.currentUser.subscribe(user => {
      this.currentUser = user;
    })
  }

  ngOnInit() {
  }

  onSubmit(event: Event) {
    event.preventDefault();
    if (this.createCommentForm.valid) {
      const values = this.createCommentForm.value;
      values.article_id = this.article.id;
      this.commentsService.create(values)
        .subscribe(this.onSuccess.bind(this), this.onFail.bind(this));
    }
  }

  onSuccess(comment: Comment) {
    this.createCommentForm.reset();
    this.commentaryCreated.emit(comment);
  }

  onFail(err: ApiError) {
    this.creationError.emit(err);
  }

  get hasPermissions() {
    if (!this.currentUser) {
      return false;
    }
    const isAuthor = this.article.author_id === this.currentUser.id;
    return isAuthor || !this.currentUser.isUser;
  }
}
