import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Article} from '../../models/article';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FileUploader} from 'ng2-file-upload';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss']
})
export class ArticleFormComponent implements OnInit {
  @Input() public article: Article;
  @Input() public title: string;
  @Output() public articleFormSubmit = new EventEmitter<FormData>();
  public articleForm: FormGroup;
  public uploader: FileUploader;

  constructor(private titleService: Title) {
    this.uploader = new FileUploader({});
  }

  ngOnInit() {
    const initialData = {
      title: (this.article) ? this.article.title : '',
      text: (this.article) ? this.article.text : '',
    };
    this.articleForm = new FormGroup({
      title: new FormControl(initialData.title, [Validators.required, Validators.maxLength(32)]),
      text: new FormControl(initialData.text, [Validators.required])
    });
    this.titleService.setTitle(this.title);
  }

  onSubmit(event: Event) {
    event.preventDefault();
    if (this.articleForm.valid && (this.currentFile || this.articleImage)) {
      const formData = new FormData();
      const values = this.articleForm.value;
      formData.append('text', values.text);
      formData.append('title', values.title);
      if (this.currentFile) {
        formData.append('image', this.currentFile._file);
      }
      this.articleFormSubmit.emit(formData);
    }
  }

  /**
   * Return current file in uploader
   * @returns {FileItem}
   */
  get currentFile() {
    return (this.uploader.queue.length) ? this.uploader.queue[0] : null;
  }

  get articleImage() {
    return this.article && this.article.image;
  }

  public onFileInputClick() {
    this.uploader.clearQueue();
  }

  hasErrors(controlName: string) {
    const control = this.articleForm.controls[controlName];
    return !control.valid && control.touched;
  }

  hasError(controlName: string, errorName: string) {
    return this.articleForm.controls[controlName].errors[errorName];
  }


}
