import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Comment} from '../../../models/comment';
import {User} from "../../../../../shared/models/user";

@Component({
  selector: '[app-comments-list-item]',
  templateUrl: './comments-list-item.component.html',
  styleUrls: ['./comments-list-item.component.scss']
})
export class CommentsListItemComponent implements OnInit {
  @Input() public comment: Comment;
  @Input() public currentUser: User;
  @Output() public deleteClick = new EventEmitter<Comment>();
  @Output() public commentUpdated = new EventEmitter<Comment>();
  public isEditorActivated: boolean;

  constructor() { }

  ngOnInit() {
  }

  onDeleteClick() {
    this.deleteClick.emit(this.comment);
  }

  onEdit(readonly: boolean) {
    this.isEditorActivated = !readonly;
  }

  onEditEnded(state: {old: string, new: string}) {
    if (state.old !== state.new) {
      this.comment.text = state.new;
      this.commentUpdated.emit(this.comment);
    }
  }

  get canDelete() {
    if (!this.currentUser) {
      return false;
    }
    const isAuthor = this.currentUser.id === this.comment.author_id;
    const isAdmin = this.currentUser.isAdmin;
    return this.currentUser && (isAdmin || isAuthor);
  }

  get canUpdate() {
    if (!this.currentUser) {
      return false;
    }
    const isAuthor = this.currentUser.id === this.comment.author_id;
    const isPremiumUser = this.currentUser.isPremiumUser;
    const isAdmin = this.currentUser.isAdmin;
    return (isAuthor && isPremiumUser) || isAdmin;
  }
}
