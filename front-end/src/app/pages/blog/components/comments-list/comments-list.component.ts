import {Component, Input, OnInit} from '@angular/core';
import {Comment} from '../../models/comment';
import {CommentsService} from '../../services/comments.service';
import {AlertsService} from '../../../../shared/services/alerts.service';
import ApiError from '../../../../shared/models/api-error';
import {AlertTypes} from '../../../../shared/models/alert-types.enum';
import {SessionService} from '../../../../shared/services/session.service';
import {User} from '../../../../shared/models/user';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss']
})
export class CommentsListComponent implements OnInit {
  @Input() public comments: Comment[];
  public currentUser: User;

  constructor(private commentsService: CommentsService,
              private alertsService: AlertsService,
              private sessionService: SessionService) {
    this.sessionService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
  }

  onDeleteClick(comment: Comment) {
    this.commentsService.delete(comment)
      .subscribe(this.onSuccess(comment), this.onFail.bind(this));
  }

  onCommentUpdated(comment: Comment) {
    this.commentsService.update(comment, {text: comment.text})
      .subscribe(this.onCommentUpdateSuccess(comment), this.onCommentUpdateFail.bind(this));
  }

  onCommentUpdateSuccess(oldComment: Comment) {
    return (newComment: Comment) => {
      const index = this.comments.indexOf(oldComment);
      this.comments[index] = newComment;
      this.alertsService.push({
        type: AlertTypes.PRIMARY,
        msg: 'Comment was updated!',
        timeout: 2000,
        dismissable: true,
      });
    };

  }

  onCommentUpdateFail(err: ApiError) {
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: 'Comment wasn\'t updated!',
      timeout: 2000,
      dismissable: true,
    });
  }

  onSuccess(deletedComment: Comment) {
    return () => {
      this.alertsService.push({
        type: AlertTypes.PRIMARY,
        msg: 'Comment was deleted',
        timeout: 5000,
        dismissable: true,
      });
      const index = this.comments.indexOf(deletedComment);
      delete this.comments[index];
    };
  }

  onFail(err: ApiError) {
    const msg = (err.message.detail) ? err.message.detail : 'Comment wasn\'t deleted!';
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: msg,
      timeout: 5000,
      dismissable: true,
    });
  }
}
