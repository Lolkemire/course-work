import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[scrollSpy]'
})
export class ScrollSpyDirective {
  @Output() public scrollPercent = new EventEmitter<number>();

  constructor(private el: ElementRef) {
  }

  /**
   * Handling window scroll event and call
   * emitPercent function when window scroll through directive element
   * @param {Event} event
   */
  @HostListener('window:scroll', ['$event']) onScroll(event: Event) {
    const topBound = this.el.nativeElement.offsetTop;
    const elementHeight = this.el.nativeElement.offsetHeight;
    const bottomBound = topBound + elementHeight;
    const pageOffset = window.pageYOffset || window.screenTop;
    if (pageOffset >= topBound && pageOffset <= bottomBound) {
      this.emitPercent(pageOffset, topBound, elementHeight);
    }
  }

  /**
   * Emit event
   * @param pageOffset
   * @param top
   * @param height
   */
  emitPercent(pageOffset, top, height) {
    const frameScrollOffset = pageOffset - top;
    const percent = frameScrollOffset * 100 / height;
    this.scrollPercent.emit(percent);
  }
}
