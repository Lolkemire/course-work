import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public frameScroll: { [key: string]: boolean };
  public toTopVisible: boolean;
  private minScroll = 1;
  private maxScroll = 80;

  constructor(private titleService: Title) {
    this.frameScroll = {};
    this.titleService.setTitle('Home');
  }

  /**
   * Handler for frame scrolling event
   * @param {string} key
   * @param {number} value
   */
  onFrameScrolling(key: string, value: number) {
    this.frameScroll[key] = value >= this.minScroll && value <= this.maxScroll;
    this.toTopVisible = key !== 'frame-1';
  }

  /**
   * Return state of frame delimiter visibility
   * @param {string} key
   * @returns {boolean}
   */
  isFrameDelimiterVisible(key: string) {
    return this.frameScroll[key];
  }
}
