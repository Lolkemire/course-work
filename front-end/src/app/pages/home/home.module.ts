import {HomeRoutingModule} from './home-routing.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {ScrollSpyDirective} from './directives/scroll-spy.directive';
import { SmoothScrollToDirective, SmoothScrollDirective } from 'ng2-smooth-scroll';


@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
  ],
  declarations: [
    HomeComponent,
    ScrollSpyDirective,
    SmoothScrollToDirective,
    SmoothScrollDirective,
  ]
})
export class HomeModule { }
