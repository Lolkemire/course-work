import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { SessionService } from '../../shared/services/session.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AuthenticationComponent implements OnInit {


  constructor(private sessionService: SessionService, private router: Router) { }

  ngOnInit() {
    this.sessionService.isAuthenticated.subscribe((state) => {
      if (state) {
        this.router.navigate(['/']);
      }
    });
  }

}
