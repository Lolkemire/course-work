import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import ApiError from '../../../shared/models/api-error';
import {AuthenticationService} from '../../../shared/services/authentication.service';
import {SessionService} from '../../../shared/services/session.service';
import {equalTo} from "../../../shared/validators/equal-to-validator";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public signUpForm: FormGroup;
  public signUpErrors: ApiError;

  constructor(private authenticationService: AuthenticationService,
              private sessionService: SessionService) {
    this.authenticationService.signupErrors.subscribe(this.onSignUpError.bind(this));
  }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32),
        equalTo('confirmPassword', 'mismatchPassword')]),
      confirmPassword: new FormControl('', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32),
        equalTo('password', 'mismatchPassword')]),
    });
  }

  onSubmit(event: Event) {
    event.preventDefault();
    this.signUpErrors = null;
    if (this.signUpForm.valid) {
      this.authenticationService.signUp(this.signUpForm.value);
    }
  }

  onSignUpError(apiError: ApiError) {
    if (apiError && apiError.message.errors) {
      const keys = Object.keys(apiError.message.errors);
      for (const errorKey of keys) {
        const error = apiError.message.errors[errorKey];
        if (error['type'] === 'unique violation') {
          const errors = this.signUpForm.controls[errorKey].errors || {};
          errors['unique'] = true;
          this.signUpForm.controls[errorKey].setErrors(errors);
        }
      }
    }
  }

  hasErrors(controlName: string) {
    const control = this.signUpForm.controls[controlName];
    return !control.valid && control.touched;
  }

  hasError(controlName: string, errorName: string) {
    return this.signUpForm.controls[controlName].errors[errorName];
  }
}
