import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../shared/services/session.service';
import { Router } from '@angular/router';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private sessionService: SessionService, private router: Router,
              private titleService: Title) {
    this.titleService.setTitle('Logout');
  }

  ngOnInit() {
    this.sessionService.clearSession();
    this.router.navigate(['/']);
  }

}
