import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import ApiError from '../../../shared/models/api-error';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import { SessionService } from '../../../shared/services/session.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  private loginErrors: ApiError;

  constructor(private authenticationService: AuthenticationService,
     private titleService: Title,
     private sessionService: SessionService) {
    this.titleService.setTitle('Log in system');
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(32)])
    });
    this.loginErrors = null;
    this.authenticationService.loginErrors.subscribe((val) => {
      this.loginErrors = val;
    });
  }

  onSubmit(event: Event) {
    event.preventDefault();
    this.loginErrors = null;
    if (this.loginForm.valid) {
      this.authenticationService.login(this.loginForm.value);
    }
  }

  get hasInvalidCredential(){
    return this.loginErrors && this.loginErrors.name === 'InvalidCredentialsError';
  }

  get invalidCredentialError() {
    return this.loginErrors && this.loginErrors.message.detail;
  }

  hasErrors(controlName: string) {
    const control = this.loginForm.controls[controlName];
    return !control.valid && control.touched;
  }

  hasError(controlName: string, errorName: string) {
    return this.loginForm.controls[controlName].errors[errorName];
  }

}
