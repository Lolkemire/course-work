export class SingUpCredentials {
    public email: string;
    public username: string;
    public password: string;
    public confirmPassword: string;
}
