import {AdminOrPremiusUserOnlyGuard} from '../shared/guards/admin-or-premius-user-only.guard';
import {PremiumUserOnlyGuard} from '../shared/guards/premius-user-only.guard';
import {AdminOnlyGuard} from '../shared/guards/admin-only.guard';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PagesComponent} from './pages.component';
import {AuthenticatedOnlyGuard} from '../shared/guards/authenticated-only.guard';
import {NotAuthenticatedOnlyGuard} from "../shared/guards/not-authenticated-only.guard";

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'settings',
        canActivate: [AuthenticatedOnlyGuard],
        loadChildren: './settings/settings.module.ts#SettingsModule',
      },
      {
        path: '',
        loadChildren: './home/home.module.ts#HomeModule',
      },
      {
        path: 'blog',
        loadChildren: './blog/blog.module.ts#BlogModule',
      }
    ]
  },
  {
    path: 'admin',
    canActivate: [AuthenticatedOnlyGuard, AdminOnlyGuard],
    loadChildren: './admin/admin.module.ts#AdminModule',
  },
  {
    path: 'authentication',
    canActivate: [NotAuthenticatedOnlyGuard, ],
    loadChildren: './authentication/authentication.module.ts#AuthenticationModule',
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PagesRoutingModule {

}
