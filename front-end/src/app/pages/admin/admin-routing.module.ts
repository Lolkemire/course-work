import {CreateUserComponent} from './components/create-user/create-user.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AdminComponent} from './admin.component';
import {UpdateUserComponent} from './components/update-user/update-user.component';
import {UsersListComponent} from './components/users-list/users-list.component';
import {RolesListResolver} from './resolvers/role-list.resolver';
import {SpecifiedUserResolver} from './resolvers/specified-user.resolver';
import {UsersListResolver} from './resolvers/user-list.resolver';
import {AdminOnlyGuard} from '../../shared/guards/admin-only.guard';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {
                path: 'users',
                resolve: {
                    users: UsersListResolver,
                    roles: RolesListResolver,
                },
                pathMatch: 'full',
                component: UsersListComponent
            },
            {
                path: 'user/:id',
                resolve: {
                    user: SpecifiedUserResolver,
                },
                canActivate: [
                    AdminOnlyGuard,
                ],
                pathMatch: 'full',
                component: UpdateUserComponent,
            },
            {
                path: 'users/create',
                component: CreateUserComponent,
                pathMatch: 'full',
                resolve: {
                    roles: RolesListResolver,
                },
                canActivate: [
                    AdminOnlyGuard
                ]
            },
            {
                path: '**',
                pathMatch: 'full',
                redirectTo: 'users',
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule {

}
