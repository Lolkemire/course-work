import { SessionService } from '../../../../shared/services/session.service';
import {ActivatedRoute} from '@angular/router';
import { UsersService } from '../../../../shared/services/users.service';
import ApiError from '../../../../shared/models/api-error';
import { User } from '../../../../shared/models/user';
import Role from '../../../../shared/models/role';
import { Component, OnInit } from '@angular/core';
import { AdminUsersService } from '../../services/admin-users.service';
import {AlertsService} from "../../../../shared/services/alerts.service";
import {AlertTypes} from "../../../../shared/models/alert-types.enum";
import {Title} from "@angular/platform-browser";


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  public usersList: User[];
  public rolesList: Role[];
  public currentUser: User;

  constructor(private route: ActivatedRoute,
              private usersService: AdminUsersService,
              private sessionService: SessionService,
              private alertsService: AlertsService,
              private titleService: Title) {
      this.sessionService.currentUser.subscribe((user) => this.currentUser = user);
      this.titleService.setTitle('Users list');
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.usersList = data['users'];
      this.rolesList = data['roles'];
    });
  }

  protected replaceUserInList(oldUser: User,newUser: User) {
    const index = this.usersList.indexOf(oldUser);
    this.usersList[index] = newUser;
  }

  onRoleChange(user: User, role: string) {
    user.role_id = parseInt(role, 10);
    // this.replaceUserInList(user, user);
  }

  onUserUpdate(user: User) {
    this.usersService.updateUser(user, user)
      .subscribe(this.onUpdateSuccess(user), this.onUpdateFail(user));
  }

  onUserDelete(user: User) {
    this.usersService.deleteUser(user)
      .subscribe(this.onDeleteSuccess(user), this.onDeleteFail(user));
  }

  onDeleteSuccess(user: User) {
    return (data: any) => {
      this.usersList = this.usersList.filter((listUser) => listUser.id !== user.id);
      this.alertsService.push({
        type: AlertTypes.PRIMARY,
        msg: `User ${user.username} was deleted!`,
        timeout: 2000,
        dismissable: true,
      });
    };
  }

  onDeleteFail(user: User) {
    return (err: ApiError) => {
      this.alertsService.push({
        type: AlertTypes.DANGER,
        msg: `User ${user.username} wasn't deleted!`,
        timeout: 2000,
        dismissable: true,
      });
    };
  }

  onUpdateSuccess(oldUser: User) {
    return (newUser) => {
      this.replaceUserInList(oldUser, newUser);
      this.alertsService.push({
        type: AlertTypes.PRIMARY,
        msg: `User ${newUser.username} was successful updated!`,
        timeout: 2000,
        dismissable: true,
      });
    };
  }

  onUpdateFail(user: User) {
    return (err: ApiError) => {
      this.alertsService.push({
        type: AlertTypes.DANGER,
        msg: `User ${user.username} wasn't updated!`,
        timeout: 2000,
        dismissable: true,
      });
    };
  }

  checkPermissions() {
    return this.currentUser.isAdmin;
  }
}
