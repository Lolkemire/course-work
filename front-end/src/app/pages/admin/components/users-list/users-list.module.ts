import { AlertModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AlertModule.forRoot(),
  ],
  declarations: [
    UsersListComponent,
  ],
  exports: [
    UsersListComponent,
  ]
})
export class UsersListModule { }
