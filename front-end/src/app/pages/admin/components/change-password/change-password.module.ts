import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {ModalModule, BsModalService,  ModalBackdropComponent} from 'ngx-bootstrap/modal';

import { ChangePasswordComponent } from './change-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
  ],
  declarations: [ChangePasswordComponent],
  exports: [ChangePasswordComponent],
})
export class ChangePasswordModule { }
