import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { User } from '../../../../shared/models/user';
import { SessionService } from '../../../../shared/services/session.service';

@Component({
  selector: 'app-admin-panel-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.scss']
})
export class AdminNavbarComponent implements OnInit {
  public isAuthenticated: boolean;
  public currentUser: User;
  public isCollapsed: boolean;


  constructor(private sessionService: SessionService) {
    this.isCollapsed = false;
    this.sessionService.isAuthenticated.subscribe(state => this.isAuthenticated = state);
    this.sessionService.currentUser.subscribe(user => this.currentUser = user);
  }

  ngOnInit() {
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.isAdmin;
  }

  get isPremium() {
    return this.currentUser && this.currentUser.isPremiumUser;
  }
}
