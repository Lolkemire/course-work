import { SessionService } from '../../../../shared/services/session.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { ChangeAvatarComponent } from '../../../../components/change-avatar/change-avatar.component';
import { ChangePasswordComponent } from '../../../../components/change-password/change-password.component';
import ApiError from '../../../../shared/models/api-error';
import { User } from '../../../../shared/models/user';
import { UsersService } from '../../../../shared/services/users.service';
import { AdminUsersService } from '../../services/admin-users.service';
import {plainToClass} from 'class-transformer';
import {AlertsService} from "../../../../shared/services/alerts.service";
import {AlertTypes} from "../../../../shared/models/alert-types.enum";
import {Title} from "@angular/platform-browser";


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  public userForm: FormGroup;
  public updatableUser: User;
  private currentAuthenticatedUser: User;

  constructor(private route: ActivatedRoute,
              private usersService: AdminUsersService,
              private sessionService: SessionService,
              private alertsService: AlertsService,
              private titleService: Title) {
    this.route.data.subscribe((data) => {
      this.updatableUser = data['user'];
    });
    this.sessionService.currentUser.subscribe(user => this.currentAuthenticatedUser = user);
    this.titleService.setTitle(`Updating user #${this.updatableUser.id} `);
  }

  ngOnInit() {
    this.userForm = new FormGroup({
      username: new FormControl(this.updatableUser.username, [Validators.required]),
      email: new FormControl(this.updatableUser.email, [Validators.required, Validators.email]),
      firstName: new FormControl(this.updatableUser.firstName),
      lastName: new FormControl(this.updatableUser.lastName),
    });
  }

  onSubmit(event: Event) {
    event.preventDefault();
    if (this.userForm.valid) {
      this.usersService.updateUser(this.updatableUser, this.userForm.value)
        .subscribe(this.onUpdateSuccess.bind(this), this.onUpdateFail.bind(this));
    }
  }

  onAvatarUpdated(user: User) {
    if (user.id === this.currentAuthenticatedUser.id) {
      this.currentAuthenticatedUser.avatar = user.avatar;
    }
    this.updatableUser.avatar = user.avatar;
    this.alertsService.push({
      type: AlertTypes.PRIMARY,
      msg: 'Avatar was updated!',
      timeout: 2000,
      dismissable: true,
    });
  }

  onPasswordChange(state: boolean) {
    if (state) {
      this.alertsService.push({
        type: AlertTypes.PRIMARY,
        msg: 'Password was updated!',
        timeout: 2000,
        dismissable: true,
      });
    } else {
      this.alertsService.push({
        type: AlertTypes.DANGER,
        msg: 'Password wasn\'t updated!',
        timeout: 2000,
        dismissable: true,
      });
    }
  }

  onUpdateSuccess(user: User) {
    this.alertsService.push({
      type: AlertTypes.PRIMARY,
      msg: 'User was updated!',
      timeout: 2000,
      dismissable: true,
    });
  }

  onUpdateFail(apiError: ApiError) {
    if (apiError.message.errors) {
      const keys = Object.keys(apiError.message.errors);
      for (const errorKey of keys) {
        const error = apiError.message.errors[errorKey];
        if (error['type'] === 'unique violation') {
          const errors = this.userForm.controls[errorKey].errors || {};
          errors['unique'] = true;
          this.userForm.controls[errorKey].setErrors(errors);
        }
      }
    }
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: 'User wasn\'t updated!',
      timeout: 2000,
      dismissable: true,
    });
  }

  hasErrors(controlName: string) {
    const control = this.userForm.controls[controlName];
    return !control.valid && control.touched;
  }

  hasError(controlName: string, errorName: string) {
    return this.userForm.controls[controlName].errors[errorName];
  }
}
