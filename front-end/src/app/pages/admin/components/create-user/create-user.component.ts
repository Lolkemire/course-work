import { DefaultRoles } from '../../../../shared/config/roles';
import { AdminUsersService } from '../../services/admin-users.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../../../shared/models/user';
import ApiError from '../../../../shared/models/api-error';
import Role from '../../../../shared/models/role';
import { ActivatedRoute } from '@angular/router';
import {AlertsService} from '../../../../shared/services/alerts.service';
import {AlertTypes} from '../../../../shared/models/alert-types.enum';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  public userForm: FormGroup;
  public roles: Role[];

  constructor(private route: ActivatedRoute,
              private userService: AdminUsersService,
              private alertsService: AlertsService,
              private titleService: Title) {
    this.route.data.subscribe(data => {
      this.roles = data['roles'];
    });
    this.titleService.setTitle('Create user');
  }

  ngOnInit() {
    this.userForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(32)]),
      role_id: new FormControl(DefaultRoles.USER.id, [Validators.required]),
      firstName: new FormControl('', []),
      lastName: new FormControl('', []),
      // username: new FormControl('', [Validators.required]),
    });
  }

  hasErrors(controlName: string) {
    const control = this.userForm.controls[controlName];
    return !control.valid && control.touched;
  }

  hasError(controlName: string, errorName: string) {
    return this.userForm.controls[controlName].errors[errorName];
  }


  onSubmit(event: Event) {
    event.preventDefault();
    if (this.userForm.valid) {
      this.userService.createUser(this.userForm.value)
        .subscribe(this.onSuccess.bind(this), this.onFail.bind(this));
    }
  }

  onSuccess(user: User) {
    this.alertsService.push({
      type: AlertTypes.PRIMARY,
      msg: `User ${user.username} created!`,
      timeout: 2000,
      dismissable: true,
    });
    this.userForm.reset();
    this.userForm.controls['role_id'].setValue(DefaultRoles.USER.id);
  }

  onFail(apiError: ApiError) {
    if (apiError.message.errors) {
      const keys = Object.keys(apiError.message.errors);
      for (const errorKey of keys) {
        const error = apiError.message.errors[errorKey];
        if (error['type'] === 'unique violation') {
          const errors = this.userForm.controls[errorKey].errors || {};
          errors['unique'] = true;
          this.userForm.controls[errorKey].setErrors(errors);
        }
      }
    }
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: `User wasn\'t created!`,
      timeout: 2000,
      dismissable: true,
    });
  }

}
