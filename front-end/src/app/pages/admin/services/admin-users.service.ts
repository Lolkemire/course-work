import { Injectable } from '@angular/core';
import ApiService from '../../../shared/services/api.service';
import { Http } from '@angular/http';
import { SessionService } from '../../../shared/services/session.service';
import { User } from '../../../shared/models/user';
import { Observable } from 'rxjs/Rx';
import {plainToClass} from "class-transformer";

@Injectable()
export class AdminUsersService extends ApiService {
  private apiPath = '/api/v1/admin/users';

  constructor(private http: Http, protected sessionService: SessionService) {
    super(sessionService);
  }

  get(id: number) {
    const path = `${this.apiPath}/${id}`;
    return this.mapRequest(this.http.get(path, { headers: this.getHeaders()}))
          .map(res => plainToClass(User, res));
  }

  getAll() {
    return this.mapRequest(this.http.get(this.apiPath, { headers: this.getHeaders()}))
      .map(res => plainToClass(User, res as Object[]));
  }

  createUser(userData: {[key: string]: any}) {
    return this.mapRequest(this.http.post(this.apiPath, userData, {headers: this.getHeaders()}))
      .map(res => plainToClass(User, res));
  }

  deleteUser(user: User) {
    const path = `${this.apiPath}/${user.id}`;
    return this.http.delete(path, {headers: this.getHeaders()})
    .map(this.mapSuccess.bind(this)).catch(this.mapFail.bind(this));
  }

  updateUser(user: User, data: {[key: string]: any}) {
    const path = `${this.apiPath}/${user.id}`;
    return this.mapRequest(this.http.patch(path, data, {headers: this.getHeaders()}))
      .map(res => plainToClass(User, res));
  }

  changePassword(user: User, data: {[key: string]: any}) {
    const path = `${this.apiPath}/${user.id}/change-password`;
    return this.mapRequest(this.http.post(path, data, {headers: this.getHeaders()}))
      .map(res => plainToClass(User, res));
  }

  public changeAvatar(user: User, formData: FormData) {
    const url = `${this.apiPath}/${user.id}/change-avatar`;
    const headers = this.getHeaders();
    headers.append('enctype', 'multipart/form-data');
    headers.append('Cache-Control', 'no-cache');
    headers.append('Cache-Control', 'no-store');
    headers.append('Pragma', 'no-cache');
    return this.mapRequest(this.http.post(url, formData, {headers: headers}))
      .map(res => plainToClass(User, res));
  }
}
