import {AdminNavbarComponent} from './components/admin-navbar/admin-navbar.component';
import {ChangePasswordModule} from './components/change-password/change-password.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AlertModule, CollapseModule} from 'ngx-bootstrap';
import {ModalModule} from 'ngx-bootstrap/modal';
import {BsDropdownModule} from 'ngx-bootstrap/ng2-bootstrap';

import {ChangeAvatarModule} from '../../components/change-avatar/change-avatar.module';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from './admin.component';
import {CreateUserComponent} from './components/create-user/create-user.component';
import {UpdateUserComponent} from './components/update-user/update-user.component';
import {UsersListModule} from './components/users-list/users-list.module';
import {RolesListResolver} from './resolvers/role-list.resolver';
import {SpecifiedUserResolver} from './resolvers/specified-user.resolver';
import {UsersListResolver} from './resolvers/user-list.resolver';
import {AdminUsersService} from './services/admin-users.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,

    CollapseModule,
    BsDropdownModule,

    AlertModule.forRoot(),
    ChangeAvatarModule,
    ChangePasswordModule,
    ModalModule.forRoot(),

    UsersListModule,
    AdminRoutingModule,
  ],
  declarations: [
    AdminComponent,
    AdminNavbarComponent,
    UpdateUserComponent,
    CreateUserComponent,
  ],
  providers: [
    UsersListResolver,
    RolesListResolver,
    SpecifiedUserResolver,
    AdminUsersService
  ],
})
export class AdminModule { }
