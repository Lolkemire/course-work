import { RolesService } from '../../../shared/services/roles.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import Role from '../../../shared/models/role';
import { User } from '../../../shared/models/user';
import { UsersService } from '../../../shared/services/users.service';


@Injectable()
export class RolesListResolver implements Resolve<Role[]> {
    constructor(private rolesService: RolesService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<Role[]> | Promise<Role[]> | Role[] {
        return this.rolesService.getAll();
    }
}
