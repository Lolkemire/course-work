import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { User } from '../../../shared/models/user';
import { UsersService } from '../../../shared/services/users.service';
import { AdminUsersService } from '../services/admin-users.service';


@Injectable()
export class UsersListResolver implements Resolve<User[]> {
    constructor(private usersService: AdminUsersService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<User[]> | Promise<User[]> | User[] {
        return this.usersService.getAll();
    }
}
