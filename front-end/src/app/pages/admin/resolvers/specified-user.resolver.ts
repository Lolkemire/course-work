import { AdminUsersService } from '../services/admin-users.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { User } from '../../../shared/models/user';
import { UsersService } from '../../../shared/services/users.service';


@Injectable()
export class SpecifiedUserResolver implements Resolve<User> {
    constructor(private usersService: AdminUsersService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<User> | Promise<User> | User {
        return this.usersService.get(route.params['id']);
    }
}
