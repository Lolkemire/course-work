import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

import { ChangeAvatarModule } from '../../components/change-avatar/change-avatar.module';
import { ChangePasswordComponent } from '../../components/change-password/change-password.component';
import { ChangePasswordModule } from '../../components/change-password/change-password.module';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { AlertModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    SettingsRoutingModule,
    ChangePasswordModule,
    ChangeAvatarModule,
    AlertModule.forRoot(),
  ],
  declarations: [SettingsComponent],
  entryComponents: [ChangePasswordComponent]
})
export class SettingsModule { }
