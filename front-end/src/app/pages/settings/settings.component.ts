import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import ApiError from '../../shared/models/api-error';
import {User} from '../../shared/models/user';
import {SessionService} from '../../shared/services/session.service';
import {UsersService} from '../../shared/services/users.service';
import {AlertsService} from '../../shared/services/alerts.service';
import {AlertTypes} from '../../shared/models/alert-types.enum';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SettingsComponent implements OnInit {
  public userForm: FormGroup;
  public currentUser: User;

  constructor(private sessionSession: SessionService, private usersService: UsersService,
              private alertsService: AlertsService,
              private titleService: Title) {
    this.sessionSession.currentUser.subscribe(data => {
      this.currentUser = data;
    });
    this.titleService.setTitle('Profile settings');
  }

  ngOnInit() {
    this.userForm = new FormGroup({
      firstName: new FormControl(this.currentUser.firstName),
      lastName: new FormControl(this.currentUser.lastName)
    });
  }

  /**
   * Perform user update on form submit
   * @param {Event} event
   */
  onSubmit(event: Event) {
    event.preventDefault();
    if (this.userForm.valid) {
      this.usersService.updateUser(this.currentUser, this.userForm.value)
        .subscribe(this.onUpdateSuccess.bind(this), this.onUpdateFail.bind(this));
    }
  }

  /**
   * Handler for password changed event
   * @param {boolean} state
   */
  onPasswordChanged(state: boolean) {
    if (state) {
      this.alertsService.push({
        type: AlertTypes.SUCCESS,
        msg: 'Password was changed!',
        timeout: 2000,
        dismissable: true,
      });
    } else {
      this.alertsService.push({
        type: AlertTypes.DANGER,
        msg: 'Password wasn\'t changed!',
        timeout: 2000,
        dismissable: true,
      });
    }
  }

  /**
   * Handler for avatar updated event
   * @param {User} user
   */
  onAvatarUpdated(user: User) {
    this.alertsService.push({
      type: AlertTypes.SUCCESS,
      msg: 'Avatar was updated!',
      timeout: 2000,
      dismissable: true,
    });
    this.currentUser.avatar = user.avatar;
    this.sessionSession.updateUser(this.currentUser);
  }

  /**
   * Handler for successful user update
   * @param {User} user
   */
  onUpdateSuccess(user: User) {
    user.avatar = user.avatar || this.currentUser.avatar;
    this.sessionSession.updateUser(user);
    this.alertsService.push({
      type: AlertTypes.SUCCESS,
      msg: 'Profile was updated!',
      timeout: 2000,
      dismissable: true,
    });
  }

  /**
   * Handler for failure user update
   * @param {ApiError} err
   */
  onUpdateFail(err: ApiError) {
    this.alertsService.push({
      type: AlertTypes.DANGER,
      msg: 'Profile wasn\'t updated!',
      timeout: 2000,
      dismissable: true,
    });
  }
}
