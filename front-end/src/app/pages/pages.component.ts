import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AlertNotification} from '../shared/models/alert-notification';
import {AlertsService} from '../shared/services/alerts.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PagesComponent implements OnInit {

  ngOnInit() {
  }

}
