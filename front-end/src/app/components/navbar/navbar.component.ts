import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { User } from '../../shared/models/user';
import { SessionService } from '../../shared/services/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public isAuthenticated: boolean;
  public currentUser: User;
  public isCollapsed: boolean;


  constructor(private sessionService: SessionService) {
    this.isCollapsed = true;
    this.sessionService.isAuthenticated.subscribe(val => {
      this.isAuthenticated = val;
    });
    this.sessionService.currentUser.subscribe(val => {
      this.currentUser = val;
    });
  }

  ngOnInit() {

  }


  public collapsed(event: any): void {
  }

  public expanded(event: any): void {
  }

  public get hasPermissions() {
    return this.currentUser.isAdmin;
  }

  public get canCreateBlog() {
    return !this.currentUser.isAdmin;
  }

}
