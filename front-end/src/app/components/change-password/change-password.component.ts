import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

import ApiError from '../../shared/models/api-error';
import {User} from '../../shared/models/user';
import {SessionService} from '../../shared/services/session.service';
import {UsersService} from '../../shared/services/users.service';
import {equalTo} from "../../shared/validators/equal-to-validator";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  @Input() public user: User;
  @Input() public buttonStyle: string;
  @Output() public passwordChanged = new EventEmitter<boolean>();

  public changePasswordForm: FormGroup;
  public error: ApiError;
  public formError: string;
  private currentUser: User;
  public modalRef: BsModalRef;

  constructor(private modalService: BsModalService, private usersService: UsersService, private sessionService: SessionService) {

  }

  ngOnInit(): void {
    this.sessionService.currentUser.subscribe((user) => {
      this.currentUser = user;
    });
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32)
      ]),
      newPassword: new FormControl('', [
        Validators.required,
        equalTo('confirmNewPassword', 'mismatchPassword'),
        Validators.minLength(6),
        Validators.maxLength(32)]),
      confirmNewPassword: new FormControl('', [
        Validators.required,
        equalTo('newPassword', 'mismatchPassword'),
        Validators.minLength(6),
        Validators.maxLength(32)
      ]),
    });
    this.buttonStyle = this.buttonStyle || 'btn-primary';
    // throw new Error("Method not implemented.");
  }

  public openModal(template: TemplateRef<any>) {
    this.modalService.config.class = 'modal-lg';
    this.modalRef = this.modalService.show(template);
  }

  public onSubmit(event: Event) {
    if (this.changePasswordForm.valid) {
      this.usersService.changePassword(this.currentUser, this.changePasswordForm.value)
        .subscribe(this.onSuccess.bind(this), this.onFail.bind(this));
    }
  }

  onSuccess(data: any) {
    this.passwordChanged.emit(true);
    this.modalRef.hide();
  }

  onFail(error: ApiError) {
    this.error = error;
    if (this.error.code === 400 && this.error.name === 'BadRequestError') {
      this.schemeValidationError(this.error.message.errors);
    } else if (this.error.code === 400 && this.error.name === 'ValidationError') {
      this.changePasswordError(this.error.message.detail);
    }
    this.passwordChanged.emit(false);
  }

  protected schemeValidationError(errors: { [key: string]: string }) {

  }

  protected changePasswordError(detail: string) {
    this.formError = detail;
  }

  hasErrors(controlName: string) {
    const control = this.changePasswordForm.controls[controlName];
    return !control.valid && control.touched;
  }

  hasError(controlName: string, errorName: string) {
    return this.changePasswordForm.controls[controlName].errors[errorName];
  }
}
