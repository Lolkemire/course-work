import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import ApiError from '../../shared/models/api-error';
import { User } from '../../shared/models/user';
import { UsersService } from '../../shared/services/users.service';

// TODO: remove changeAvatarForm

@Component({
  selector: 'app-change-avatar',
  templateUrl: './change-avatar.component.html',
  styleUrls: ['./change-avatar.component.scss']
})
export class ChangeAvatarComponent implements OnInit {
  @Input() public user: User;
  @Input() public buttonStyle: string;
  @Output() public avatarUpdated = new EventEmitter<User>();

  public uploader: FileUploader;
  public error: ApiError;
  public formError: string;
  public modalRef: BsModalRef;

  constructor(private modalService: BsModalService,
    private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.uploader = new FileUploader({});
    this.buttonStyle = this.buttonStyle || 'btn-primary';
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  public onSubmit(event: Event) {
    if (this.uploader.queue.length) {
      const formData = new FormData();
      formData.append('avatar', this.uploader.queue[0]._file);
      this.usersService.changeAvatar(this.user, formData)
        .subscribe(this.onSuccess.bind(this), this.onFail.bind(this));
    }
  }

  /**
   * Clear uploader queue every time when click on file input
   */
  public onFileInputClick() {
    this.uploader.clearQueue();
  }

  onSuccess(data: any) {
    this.avatarUpdated.emit(data);
    this.modalRef.hide();
    this.clearModal();
  }

  onFail(error: ApiError) {
    this.error = error;
  }

  protected clearModal() {
    this.uploader.clearQueue();
    this.error = null;
    this.formError = null;
  }

  get currentFile() {
    return (this.uploader.queue.length) ? this.uploader.queue[0] : null;
  }
}
