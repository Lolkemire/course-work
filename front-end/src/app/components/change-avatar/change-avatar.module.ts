import {ModalModule} from 'ngx-bootstrap/modal';
import {ChangeAvatarComponent} from './change-avatar.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from 'ng2-file-upload';
import {ImagePreview} from "../../image-preview/directives/imagepreview.directive";
import {ImagePreviewModule} from "../../image-preview/image-preview.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ImagePreviewModule,
    ReactiveFormsModule,
    FileUploadModule,
    ModalModule,
  ],
  declarations: [
    ChangeAvatarComponent,
  ],
  exports: [ChangeAvatarComponent],
})
export class ChangeAvatarModule { }
