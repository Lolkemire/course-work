import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthenticationService} from './shared/services/authentication.service';
import {AlertsService} from './shared/services/alerts.service';
import {AlertNotification} from './shared/models/alert-notification';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  public alerts: AlertNotification[];

  constructor(private alertsService: AlertsService, private authService: AuthenticationService) {
    this.alerts = [];
    this.alertsService.alerts.subscribe(value => {
      this.alerts.push(value);
    });
  }

  ngOnInit(): void {
    this.authService.loadSavedSession();
  }
}
