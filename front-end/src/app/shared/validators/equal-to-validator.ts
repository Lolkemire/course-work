import { FormControl } from '@angular/forms';

/**
 * Validate values of current validator and target
 * @param {string} toField
 * @param {string} errorName
 * @returns {(input: FormControl) => any}
 */
export function equalTo(toField: string, errorName: string) {
  return (input: FormControl) => {
    const value = input.value;
    const target = input.root.get(toField);

    if (target && value !== target.value) {
      target.setErrors({
        [errorName]: true
      });
    }

    if (target && value === target.value) {
      delete target.errors[errorName];
      input.root.updateValueAndValidity();
      if (!Object.keys(target.errors).length) {
        target.setErrors(null);
      }
    }


    return null;
  };
}
