import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {SessionService} from '../services/session.service';

/**
 * Guard limit access to route for admin
 *
 * @export
 * @class NotAdminGuard
 * @implements {CanActivate}
 */
@Injectable()
export class NotAdminGuard implements CanActivate {
  constructor(private sessionService: SessionService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Observable<boolean>(observer => {
      this.sessionService.currentUser.subscribe(user => {
        if (user && !user.isAdmin) {
          observer.next(true);
        } else {
          this.router.navigate(['/']);
          observer.next(false);
        }
      });
    });
  }
}
