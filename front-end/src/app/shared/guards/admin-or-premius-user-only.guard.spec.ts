import { TestBed, async, inject } from '@angular/core/testing';

import { AdminOrPremiusUserOnlyGuard } from './admin-or-premius-user-only.guard';

describe('AdminOrPremiusUserOnlyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminOrPremiusUserOnlyGuard]
    });
  });

  it('should ...', inject([AdminOrPremiusUserOnlyGuard], (guard: AdminOrPremiusUserOnlyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
