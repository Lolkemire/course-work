import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {SessionService} from '../services/session.service';
import {Observer} from 'rxjs/Observer';

/**
 * Guard limit access to route for not admin or premium user
 *
 * @export
 * @class AdminOrPremiusUserOnlyGuard
 * @implements {CanActivate}
 */
@Injectable()
export class AdminOrPremiusUserOnlyGuard implements CanActivate {
  constructor(private sessionService: SessionService, private router: Router) {

  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isAdminOrPremiumUser();
  }

  protected isAdminOrPremiumUser() {
    return new Observable<boolean>((observer) => {
      this.sessionService.currentUser.subscribe((val) => {
        if (val) {
          if (val.isAdmin || val.isPremiumUser) {
            observer.next(true);
          } else {
            this.navigateHome(observer);
          }
        } else {
          this.navigateHome(observer);
        }
      });
    });
  }

  protected navigateHome(observer: Observer<boolean>) {
    this.router.navigate(['/']);
    observer.next(false);
  }
}
