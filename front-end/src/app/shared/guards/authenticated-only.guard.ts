import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionService } from '../services/session.service';

/**
 * Guard limit access to route for not authenticated
 *
 * @export
 * @class AuthenticatedOnlyGuard
 * @implements {CanActivate}
 */
@Injectable()
export class AuthenticatedOnlyGuard implements CanActivate {
  constructor(private sessionService: SessionService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.sessionService.authenticated) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
