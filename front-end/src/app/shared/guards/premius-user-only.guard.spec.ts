import { TestBed, async, inject } from '@angular/core/testing';

import { PremiumUserOnlyGuard } from './premius-user-only.guard';

describe('PremiusUserOnlyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PremiumUserOnlyGuard]
    });
  });

  it('should ...', inject([PremiumUserOnlyGuard], (guard: PremiumUserOnlyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
