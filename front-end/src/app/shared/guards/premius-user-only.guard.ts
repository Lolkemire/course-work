import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionService } from '../services/session.service';

/**
 * Guard limit access to route for not premium user
 *
 * @export
 * @class PremiumUserOnlyGuard
 * @implements {CanActivate}
 */
@Injectable()
export class PremiumUserOnlyGuard implements CanActivate {
  constructor(private sessionService: SessionService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isPremiumUser();
  }

  protected isPremiumUser() {
    return new Observable<boolean>((observer) => {
      this.sessionService.currentUser.subscribe((val) => {
        if (val) {
          observer.next(val.isPremiumUser);
        } else {
          this.router.navigate(['/']);
          observer.next(false);
        }
      });
    });
  }
}
