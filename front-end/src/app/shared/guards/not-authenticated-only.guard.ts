import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

/**
 * Guard limit access to route for authenticated users
 *
 * @export
 * @class NotAuthenticatedOnlyGuard
 * @implements {CanActivate}
 */
@Injectable()
export class NotAuthenticatedOnlyGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
}
