import { TestBed, async, inject } from '@angular/core/testing';

import { NotAuthenticatedOnlyGuard } from './not-authenticated-only.guard';

describe('NotAuthenticatedOnlyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotAuthenticatedOnlyGuard]
    });
  });

  it('should ...', inject([NotAuthenticatedOnlyGuard], (guard: NotAuthenticatedOnlyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
