import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { SessionService } from '../services/session.service';


/**
 * Guard limit access to route for not admin
 *
 * @export
 * @class AdminOnlyGuard
 * @implements {CanActivate}
 */
@Injectable()
export class AdminOnlyGuard implements CanActivate {
  constructor(private sessionService: SessionService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isAdmin();
  }

  /**
   * Perform permission check
   * @returns {Observable<boolean>}
   */
  protected isAdmin() {
    return new Observable<boolean>((observer) => {
      this.sessionService.currentUser.subscribe((val) => {
        if (val) {
          observer.next(val.isAdmin);
        } else {
          this.router.navigate(['/']);
          observer.next(false);
        }
      });
    });
  }
}
