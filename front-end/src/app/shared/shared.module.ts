import {AdminOrPremiusUserOnlyGuard} from './guards/admin-or-premius-user-only.guard';
import {PremiumUserOnlyGuard} from './guards/premius-user-only.guard';
import {RolesService} from './services/roles.service';
import {UsersService} from './services/users.service';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {AdminOnlyGuard} from './guards/admin-only.guard';
import {AuthenticatedOnlyGuard} from './guards/authenticated-only.guard';
import {AuthenticationService} from './services/authentication.service';
import {SessionService} from './services/session.service';
import {NotAdminGuard} from './guards/not-admin.guard';
import {AlertsService} from './services/alerts.service';
import {NotAuthenticatedOnlyGuard} from './guards/not-authenticated-only.guard';

@NgModule({
  imports: [
    CommonModule
  ],

  providers: [
    // SERVICES
    AuthenticationService,
    SessionService,
    UsersService,
    RolesService,
    AlertsService,
    // GUARDS
    AdminOnlyGuard,
    NotAdminGuard,
    NotAuthenticatedOnlyGuard,
    PremiumUserOnlyGuard,
    AdminOrPremiusUserOnlyGuard,
    AuthenticatedOnlyGuard,
  ],
})
export class SharedModule { }
