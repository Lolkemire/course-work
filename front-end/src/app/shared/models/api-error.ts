/**
 * API response error
 */
export default class ApiError {
    code: number;
    name: string;
    message: {
      errors?: { [key: string]: any };
      detail?: string;
    };

    constructor(code: number, data: {[key: string]: any}) {
        this.message = {};
        this.code = code;
        this.name = data['name'];
        this.message.errors = data['message']['errors'];
        this.message.detail = data['message']['detail'];
    }
}
