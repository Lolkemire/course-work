/**
 * User role
 */
export default class Role {
    id: number;
    name: string;
}
