import {AlertTypes} from './alert-types.enum';

/**
 * Interface of alert notification
 */
export interface AlertNotification {
  type: AlertTypes;
  msg: string;
  timeout: number;
  dismissable: boolean;
}
