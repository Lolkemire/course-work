import {User} from './user';

/**
 * Authentication result
 */
export class AuthenticationResult {
  token: string;
  user: User;
}
