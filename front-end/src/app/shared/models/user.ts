import {DefaultRoles} from '../config/roles';
import Role from './role';

/**
 * User model
 */
export class User {
  id: number;
  username: string;
  email: string;
  password: string;
  role_id: number;
  role: Role;
  firstName: string;
  lastName: string;
  private _avatar: string;
  createdAt: Date;
  updatedAt: Date;

  /**
   * Returns user avatar or default avatar
   * @returns {string}
   */
  get avatar() {
    return (this._avatar) ? this._avatar : '/media/users/default-avatar.png';
  }

  /**
   * Set user avatar
   * @param {string} value
   */
  set avatar(value: string) {
    this._avatar = value;
  }

  /**
   * Check is user role "admin"
   * @returns {Role | boolean}
   */
  get isAdmin() {
    return this.role && this.role.name === DefaultRoles.ADMIN.name;
  }

  /**
   * Check is user role "premium user"
   * @returns {Role | boolean}
   */
  get isPremiumUser() {
    return this.role && this.role.name === DefaultRoles.PREMIUM.name;
  }

  /**
   * Check is user role "user"
   * @returns {Role | boolean}
   */
  get isUser() {
    return this.role && this.role.name === DefaultRoles.USER.name;
  }
}
