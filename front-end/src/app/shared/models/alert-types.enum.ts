/**
 * Alert notification types
 */
export enum AlertTypes {
  PRIMARY = <any>'primary',
  SECONDARY = <any>'secondary',
  SUCCESS = <any>'success',
  DANGER = <any>'danger',
  INFO = <any>'info',
  WARNING = <any>'warning',
  LIGHT = <any>'light',
  DARK = <any>'dark',
}
