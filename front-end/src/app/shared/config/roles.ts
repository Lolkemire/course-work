export const DefaultRoles = {
    ADMIN: {
        id: 1,
        name: 'admin',
    },
    USER: {
        id: 2,
        name: 'user',
    },
    PREMIUM: {
        id: 3,
        name: 'premium user',
    },
};
