import ApiService from './api.service';
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {BehaviorSubject, Observable} from 'rxjs/Rx';

import {LoginCredentials} from '../../pages/authentication/models/login-credentials';
import {SingUpCredentials} from '../../pages/authentication/models/sing-up-credentials';
import ApiError from '../models/api-error';
import {SessionService} from './session.service';
import {plainToClass} from 'class-transformer';
import {AuthenticationResult} from '../models/authentication-result';

@Injectable()
export class AuthenticationService extends ApiService {
  private loginErrorsSubject = new BehaviorSubject<ApiError>(null);
  private signupErrorsSubject = new BehaviorSubject<ApiError>(null);

  public loginErrors = this.loginErrorsSubject.asObservable();
  public signupErrors = this.signupErrorsSubject.asObservable();

  constructor(private http: Http, protected sessionService: SessionService) {
    super(sessionService);
  }

  /**
   * Perform user authentication in system
   * @param {LoginCredentials} credentials user credentials
   */
  login(credentials: LoginCredentials): void {
    this.mapRequest(this.http.post('/api/v1/auth/login', credentials))
      .map(res => plainToClass(AuthenticationResult, res))
      .subscribe(this.onSuccess.bind(this), this.onLoginFail.bind(this));
  }

  /**
   * Perform user registration in system
   * @param {SingUpCredentials} credentials
   */
  signUp(credentials: SingUpCredentials) {
    this.mapRequest(this.http.post('/api/v1/auth/registration', credentials))
      .map(res => plainToClass(AuthenticationResult, res))
      .subscribe(this.onSuccess.bind(this), this.onSignUpFail.bind(this));
  }

  /**
   * Perform token refreshing
   * @param {string} token
   */
  refreshToken(token: string): void {
    this.mapRequest(this.http.post('/api/v1/auth/refresh-token', {token}))
      .map(res => plainToClass(AuthenticationResult, res))
      .subscribe(this.onSuccess.bind(this), this.onRefreshTokenFail.bind(this));
  }

  /**
   * Extract session information from storage
   * and perform refresh token if exists
   */
  public loadSavedSession() {
    const session = this.sessionService.loadFromStorage();
    if (session.token) {
      this.refreshToken(session.token);
    }
  }

  /**
   * On operation success save session data
   * @param {AuthenticationResult} data
   */
  onSuccess(data: AuthenticationResult) {
    this.signupErrorsSubject.next(null);
    this.loginErrorsSubject.next(null);
    this.sessionService.setUserSession(data.token, data.user, true);
  }

  /**
   * On login fail push error to subject
   * @param {ApiError} err
   */
  onLoginFail(err: ApiError) {
    this.loginErrorsSubject.next(err);
  }

  /**
   * On sign up fail push error to subject
   * @param {ApiError} err
   */
  onSignUpFail(err: ApiError) {
    this.signupErrorsSubject.next(err);
  }

  /**
   * On refresh token fail clear saved session
   * @param {ApiError} err
   */
  onRefreshTokenFail(err: ApiError) {
    this.sessionService.clearSession();
  }

}
