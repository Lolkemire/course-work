import { Injectable } from '@angular/core';
import {AlertNotification} from '../models/alert-notification';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AlertsService {
  private alertsSubject = new Subject<AlertNotification>();
  public alerts = this.alertsSubject.asObservable();

  constructor() { }

  /**
   * Push notification
   * @param {AlertNotification} notification
   */
  public push(notification: AlertNotification) {
    this.alertsSubject.next(notification);
  }

}
