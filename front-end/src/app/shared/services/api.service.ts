import {Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import ApiError from '../models/api-error';
import {SessionService} from './session.service';

export default class ApiService {
  constructor(protected sessionService: SessionService) {

  }

  /**
   * Apply map and catch functions for observable
   * @param {Observable<any>} observable
   * @returns {Observable<any | any>}
   */
  mapRequest(observable: Observable<any>) {
    return observable.map(this.mapSuccess.bind(this)).catch(this.mapFail.bind(this));
  }

  /**
   * Just call json method
   * @param {Response} res
   * @returns {Promise<any>} parsed json body or null
   */
  mapSuccess(res: Response) {
    return res.json() || null;
  }

  /**
   * Parse error response and throw it
   * @param {Response} err
   * @returns {ErrorObservable}
   */
  mapFail(err: Response) {
    return Observable.throw(this.parseError(err));
  }

  /**
   * Perform error parsing
   * @param {Response} res
   * @returns {ApiError}
   */
  parseError(res: Response) {
    const error = res.json();
    return new ApiError(res.status, error);
  }

  /**
   * Returns default headers for requests
   * @returns {Headers}
   */
  getHeaders(): Headers {
    const headers = new Headers();
    headers.set('Authorization', `Bearer ${this.sessionService.token}`);
    return headers;
  }
}
