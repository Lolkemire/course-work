import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import Role from '../models/role';
import ApiService from './api.service';
import { SessionService } from './session.service';
import {plainToClass} from 'class-transformer';

@Injectable()
export class RolesService extends ApiService {
  private apiPath = '/api/v1/roles';

  constructor(private http: Http, protected sessionService: SessionService) {
    super(sessionService);
  }

  /**
   * Perform fetching roles from api
   * @returns {Observable<Role[]>} Array of roles
   */
  public getAll() {
    return this.mapRequest(this.http.get(this.apiPath, {headers: this.getHeaders()}))
    .map(res => plainToClass(Role, res as Object[]));
  }

}
