import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user';
import {SessionService} from './session.service';
import ApiService from './api.service';
import {plainToClass} from 'class-transformer';

@Injectable()
export class UsersService extends ApiService {
  private apiPath = '/api/v1/users';

  constructor(private http: Http, protected sessionService: SessionService) {
    super(sessionService);
  }

  /**
   * Fetching user with assigned id
   * @param {number} id
   * @returns {Observable<User>}
   */
  public get(id: number): Observable<User> {
    const path = `${this.apiPath}/${id}`;
    return this.mapRequest(this.http.get(path, {headers: this.getHeaders()}))
      .map(res => plainToClass(User, res));
  }

  /**
   * Fetching all users
   * @returns {Observable<User[]>}
   */
  public getAll(): Observable<User[]> {
    return this.mapRequest(this.http.get(this.apiPath, {headers: this.getHeaders()}))
      .map(res => plainToClass(User, res as Object[]));
  }

  /**
   * Perform user updating
   * @param {User} user
   * @param {{[p: string]: any}} updateData
   * @returns {Observable<User[]>}
   */
  public updateUser(user: User, updateData: { [key: string]: any }) {
    const path = `${this.apiPath}/${user.id}`;
    return this.mapRequest(this.http.patch(path, updateData, {headers: this.getHeaders()}))
      .map(res => plainToClass(User, res));
  }

  /**
   * Perform passwod changin for specific user
   * @param {User} user
   * @param passwordsData
   * @returns {Observable<User[]>}
   */
  public changePassword(user: User, passwordsData: any) {
    const path = `${this.apiPath}/${user.id}/change-password`;
    return this.mapRequest(this.http.post(path,
      passwordsData, {headers: this.getHeaders()}))
      .map(res => plainToClass(User, res));
  }

  /**
   * Perform avatar changing
   * @param {User} user
   * @param {FormData} formData
   * @returns {Observable<User[]>}
   */
  public changeAvatar(user: User, formData: FormData) {
    const path = `/api/v1/users/${user.id}/change-avatar`;
    const headers = this.getHeaders();
    headers.append('enctype', 'multipart/form-data');
    headers.append('Cache-Control', 'no-cache');
    headers.append('Cache-Control', 'no-store');
    headers.append('Pragma', 'no-cache');
    return this.mapRequest(this.http.post(path, formData, {headers: headers}))
      .map(res => plainToClass(User, res));
  }

}
