import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { BehaviorSubject } from 'rxjs/Rx';

import { User } from '../models/user';
import {plainToClass} from 'class-transformer';

@Injectable()
export class SessionService {
  private tokenSubject = new BehaviorSubject<string>(null);
  private currentUserSubject = new BehaviorSubject<User>(null);
  private isAuthenticatedSubject = new BehaviorSubject<boolean>(false);

  public currentUser = this.currentUserSubject.asObservable();
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor(private cookieService: CookieService) { }

  /**
   * Save session data to storage, and push it subject
   * @param {string} token
   * @param {User} user
   * @param {boolean} isAuthenticated
   */
  public setUserSession(token: string, user: User, isAuthenticated: boolean) {
    this.saveToStorage(token, user);
    this.tokenSubject.next(token);
    this.currentUserSubject.next(plainToClass(User, user));
    this.isAuthenticatedSubject.next(isAuthenticated);
  }

  /**
   * Perform updating user information in storage and subjects
   * @param {User} user
   */
  public updateUser(user: User) {
    this.saveToStorage(this.tokenSubject.value, user);
    this.currentUserSubject.next(user);
  }

  /**
   * Perform saving into cookie storage
   * @param {string} token
   * @param {User} user
   */
  protected saveToStorage(token: string, user: User) {
    this.cookieService.put('access-token', token);
    this.cookieService.putObject('current-user', user);
  }

  /**
   * Clear session
   */
  public clearSession() {
    this.setUserSession('', null, false);
  }

  /**
   * Load token and user information from storage
   * @returns {{token: string; currentUser: Object}}
   */
  public loadFromStorage() {
    const token = this.cookieService.get('access-token');
    const currentUser = this.cookieService.getObject('current-user');
    return { token, currentUser };
  }

  /**
   * Return authentication status
   * @returns {boolean}
   */
  get authenticated() {
    return this.isAuthenticatedSubject.value;
  }

  /**
   * Return token
   * @returns {string}
   */
  get token() {
    return this.tokenSubject.value;
  }

}
