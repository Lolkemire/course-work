import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {CookieModule} from 'ngx-cookie';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {SharedModule} from './shared/shared.module';
import {AlertModule} from 'ngx-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
    CookieModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    // AuthenticationService,
    // SessionService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
