import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImagePreview} from "./directives/imagepreview.directive";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ImagePreview,
  ],
  exports: [
    ImagePreview
  ],
})
export class ImagePreviewModule { }
