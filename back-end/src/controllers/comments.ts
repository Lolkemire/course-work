import {
    Authorized,
    Body,
    CurrentUser, Delete,
    ForbiddenError, Get,
    HttpCode,
    JsonController,
    NotFoundError, OnNull, OnUndefined,
    Param,
    Patch,
    Post
} from 'routing-controllers';
import User from '../database/entities/User';
import {CreateComment} from '../models/comments/createComment';
import {Article} from '../database/entities/Article';
import {DefaultRoles} from '../config/app';
import {UpdateComment} from '../models/comments/updateComment';
import {Comment} from '../database/entities/Comment';


@JsonController('/api/v1/comments')
export class CommentsController {

    @Post('/')
    @HttpCode(201)
    @Authorized([DefaultRoles.USER, DefaultRoles.PREMIUM, DefaultRoles.ADMIN])
    public async createComment(@CurrentUser() user: User,
                               @Body() data: CreateComment) {
        const article = await  this.findArticleById(data.article_id);
        const isUserNotArticleAuthor = article.author_id !== user.id;
        const isUserRoleUser = user.role.name === DefaultRoles.USER;
        if (isUserRoleUser && isUserNotArticleAuthor) {
            throw new ForbiddenError(`User with role "${DefaultRoles.USER}" can comment only own articles`);
        }
        if (!user.isAdmin || !data.author_id) {
            data.author_id = user.id;
        }
        let comment: Comment = await Comment.create(data) as Comment;
        comment = await comment.reload({include: [{model: User, as: 'author'}]}) as Comment;
        return comment.toPlain();
    }

    @Patch('/:comment_id')
    @Authorized([DefaultRoles.PREMIUM, DefaultRoles.ADMIN])
    public async updateComment(@Param('comment_id') commentId: number,
                               @Body() data: UpdateComment,
                               @CurrentUser() user: User) {
        const comment = await this.findCommentById(commentId);
        const isNotCommentAuthor = user.id !== comment.author_id;
        if (user.isPremium && isNotCommentAuthor) {
            throw new ForbiddenError(`Users with role "${DefaultRoles.PREMIUM}" cannot update comments of
            anothers users`);
        }
        await comment.update(data);
        return comment.toPlain();
    }

    @Delete('/:id')
    @Authorized([DefaultRoles.PREMIUM, DefaultRoles.ADMIN, DefaultRoles.USER])
    @OnNull(200)
    @OnUndefined(200)
    public async deleteComment(@Param('id') id: number,
                               @CurrentUser() user: User) {
        const comment = await this.findCommentById(id);
        const isNotCommentAuthor = user.id !== comment.author_id;
        if (isNotCommentAuthor && !user.isAdmin) {
            throw new ForbiddenError(`Users with role "${user.role.name}" can delete only own comments!`);
        }
        await comment.destroy();
        return {};
    }

    @Get('/')
    @Authorized([DefaultRoles.ADMIN, DefaultRoles.PREMIUM, DefaultRoles.ADMIN])
    public async getAll() {
        const comments = await Comment.all({include: [{model: User, as: 'author'}]}) as Comment[];
        return comments.map(comment => comment.toPlain());
    }

    @Get('/:id')
    @Authorized([DefaultRoles.ADMIN, DefaultRoles.PREMIUM, DefaultRoles.ADMIN])
    public async get(@Param('id') id: number) {
        const comment = await this.findCommentById(id);
        return comment.toPlain();
    }

    protected async findArticleById(id: number) {
        const article = await Article.findById(id, {include: [{model: User, as: 'author'}]}) as Article;
        if (!article) {
            throw new NotFoundError(`Article with id "${id}" not found!`);
        }
        return article;
    }

    protected async findCommentById(id: number) {
        const comment = await Comment.findById(id, {include: [{model: User, as: 'author'}]}) as Comment;
        if (!comment) {
            throw new NotFoundError(`Comment with id "${id}" not found!`);
        }
        return comment;
    }


}