import {Controller, Get, Render} from 'routing-controllers';

@Controller('*')
export class HomeController {

  @Get('*')
  @Render('home.pug')
  public home() {
    return {};
  }
}