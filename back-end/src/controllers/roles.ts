import {Get, JsonController} from 'routing-controllers';
import Role from '../database/entities/Role';

@JsonController('/api/v1/roles')
export class RolesController {

    @Get('/')
    public async getAll() {
        let roles = await Role.all() as Role[];
        roles = roles.map((role: Role) => role.get({plain: true}));
        return roles;
    }
}