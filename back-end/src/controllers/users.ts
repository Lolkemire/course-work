import {avatarUpload} from '../utills/multer-upload';
import {DefaultRoles} from '../config/app';
import {
    Authorized,
    Body,
    CurrentUser,
    Get,
    JsonController,
    Param,
    Patch,
    Post,
    UploadedFile,
} from 'routing-controllers';

import ChangePassword from '../models/authentication/changePassword';
import UpdateProfile from '../models/users/updateProfile';
import User from '../database/entities/User';
import {ForbiddenError} from '../errors/ForbiddenError';
import {ObjectDoesNotExistsError} from '../errors/ObjectDoesNotExistsError';
import {UserService} from '../services/user';


@JsonController('/api/v1/users')
export class UsersController {
    constructor(private userService: UserService) {

    }

    @Get('/current')
    @Authorized([DefaultRoles.ADMIN, DefaultRoles.USER, DefaultRoles.PREMIUM])
    public getCurrentUser(@CurrentUser() user: User) {
        return user.getUserWithoutPassword();
    }

    @Get('/:id')
    @Authorized([DefaultRoles.ADMIN, DefaultRoles.USER, DefaultRoles.PREMIUM])
    public async getUser(@Param('id') id: number) {
        const user: User = await this.findUserById(id);
        return user.getUserWithoutPassword();
    }

    @Patch('/:id')
    @Authorized([DefaultRoles.ADMIN, DefaultRoles.USER, DefaultRoles.PREMIUM])
    public async updateUser(@Param('id') id: number, @Body() data: UpdateProfile) {
        let userToUpdate: User = await this.findUserById(id);
        if (userToUpdate.role.name === DefaultRoles.ADMIN) {
            throw new ForbiddenError({
                detail: `Users with role "${DefaultRoles.PREMIUM}" not allowed updating
            users with "${DefaultRoles.ADMIN}"`
            });
        }
        userToUpdate = await userToUpdate.update(data) as User;
        return userToUpdate.getUserWithoutPassword();
    }


    @Post('/:id/change-avatar')
    @Authorized([DefaultRoles.ADMIN, DefaultRoles.USER, DefaultRoles.PREMIUM])
    public async changeAvatar(@Param('id') id: number,
                              @UploadedFile('avatar', {options: avatarUpload}) avatar: any) {
        let user: User = await this.findUserById(id);
        user = await this.userService.updateAvatar(user, avatar);
        return user.getUserWithoutPassword();
    }


    @Post('/:id/change-password')
    @Authorized([DefaultRoles.ADMIN, DefaultRoles.USER, DefaultRoles.PREMIUM])
    public async changePassword(@Param('id') id: number, @Body() data: ChangePassword) {
        let user: User = await this.findUserById(id);
        user = await this.userService.changePassword(user, data.newPassword, data.oldPassword);
        return user.getUserWithoutPassword();
    }

    async findUserById(id: number) {
        const user: User = await User.findUserById(id) as User;
        if (!user) {
            throw new ObjectDoesNotExistsError({detail: `User with id ${id} not found`});
        }
        return user;
    }
}