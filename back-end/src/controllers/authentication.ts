import RegistrationCredentials from '../models/authentication/registrationCredentials';
import LoginCredentials from '../models/authentication/loginCredentials';
import {Body, HttpCode, JsonController, Post} from 'routing-controllers';
import {AuthenticationService} from '../services/authentication';
import RefreshToken from '../models/authentication/refreshToken';


@JsonController('/api/v1/auth')
export class AuthenticationController {

    constructor(private authenticationService: AuthenticationService) {

    }

    @Post('/login')
    async login(@Body() credentials: LoginCredentials) {
        return await this.authenticationService.loginByEmail(credentials);
    }

    @Post('/registration')
    @HttpCode(201)
    async registration(@Body() credentials: RegistrationCredentials) {
        return await this.authenticationService.registrationByEmail(credentials);
    }

    @Post('/refresh-token')
    async refreshToken(@Body() token: RefreshToken) {
        return await this.authenticationService.refreshToken(token);
    }
}