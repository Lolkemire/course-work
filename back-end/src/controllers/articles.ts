import User from '../database/entities/User';
import {CreateArticle} from '../models/articles/createArticle';
import {
    Authorized,
    Body,
    CurrentUser,
    Delete,
    ForbiddenError, Get,
    HttpCode,
    JsonController,
    NotFoundError,
    OnNull,
    OnUndefined,
    Param,
    Patch,
    Post,
    UploadedFile
} from 'routing-controllers';
import {DefaultRoles} from '../config/app';
import {articleImageUpload} from '../utills/multer-upload';
import {Article} from '../database/entities/Article';
import {configs} from '../config/index';
import {UpdateArticle} from '../models/articles/updateArticle';
import {ArticlesService} from '../services/articles';
import {Comment} from '../database/entities/Comment';


const appConfig = configs.getAppConfig();

@JsonController('/api/v1/articles')
export class ArticlesController {

    constructor(private articlesService: ArticlesService) {

    }

    @Get('/')
    public async getAll() {
        const articles = await Article.all({include: [{model: User, as: 'author'}], order: [
            ['createdAt', 'DESC']
        ]}) as Article[];
        return articles.map(article => article.toPlain());
    }

    @Get('/:id')
    public async get(@Param('id') id: number) {
        const article = await this.findById(id);
        return article.toPlain();
    }

    @Get('/:id/comments')
    public async getComments(@Param('id') id: number) {
        const comments = await Comment
            .findAll({where: {article_id: id}, include: [{model: User, as: 'author'}], order: [
                ['createdAt', 'DESC']
            ]}) as Comment[];
        return comments.map(comment => comment.toPlain());
    }

    @Post('/')
    @HttpCode(201)
    @Authorized([DefaultRoles.USER, DefaultRoles.PREMIUM])
    public async createArticle(@Body() data: CreateArticle,
                               @CurrentUser() currentUser: User,
                               @UploadedFile('image', {options: articleImageUpload}) image: any) {
            if (!currentUser.isAdmin || !data.author_id) {
                data.author_id = currentUser.id;
            }
            let newArticle = await this.articlesService.createArticle(data, image);
            newArticle = await newArticle.reload({include: [{model: User, as: 'author'}]});

            return newArticle.toPlain();
    }

    @Patch('/:id')
    @Authorized([DefaultRoles.USER, DefaultRoles.PREMIUM, DefaultRoles.ADMIN])
    public async updateArticle(@Body() data: UpdateArticle,
                               @CurrentUser() currentUser: User,
                               @UploadedFile('image', {options: articleImageUpload}) image: any,
                               @Param('id') id: number) {
        const article: Article = await this.findById(id);
        const isCurrentUserNotAdmin = currentUser.role.name !== DefaultRoles.ADMIN;
        const isCurrentUserNotArticleOwner = currentUser.id !== article.author_id;
        if (isCurrentUserNotArticleOwner && isCurrentUserNotAdmin) {
            throw new ForbiddenError(`User "${currentUser.username}" is not owner of "${article}" article`);
        }
        await this.articlesService.updateArticle(article, data, image);
        return article.toPlain();
    }

    @Delete('/:id')
    @Authorized([DefaultRoles.ADMIN])
    @OnUndefined(200)
    @OnNull(200)
    public async deleteArticle(@Param('id') id: number) {
        const article: Article = await this.findById(id);
        await this.articlesService.deleteArticle(article);
        return {};
    }

    protected async findById(id: number) {
        const article = await Article.findById(id, {include: [{model: User, as: 'author'}]}) as Article;
        if (!article) {
            throw new NotFoundError(`Article with id "${id}" not found!`);
        }
        return article;
    }

}