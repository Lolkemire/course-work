import {
    Authorized,
    Body,
    Delete,
    Get,
    HttpCode,
    JsonController,
    OnNull,
    OnUndefined,
    Param,
    Patch,
    Post
} from 'routing-controllers';
import {DefaultRoles} from '../../config/app';
import CreateUser from '../../models/users/createUser';
import UpdateUser from '../../models/users/updateUser';
import ChangePasswordByAdmin from '../../models/authentication/changePasswordByAdmin';
import {UserService} from '../../services/user';
import User from '../../database/entities/User';
import {ObjectDoesNotExistsError} from '../../errors/ObjectDoesNotExistsError';
import Role from '../../database/entities/Role';
import {Article} from '../../database/entities/Article';
import {ArticlesService} from '../../services/articles';

@JsonController('/api/v1/admin/users')
export class AdminUsersController {
    constructor(private userService: UserService, private articleService: ArticlesService) {

    }

    @Get('/')
    @Authorized([DefaultRoles.ADMIN])
    public async getAll() {
        const users = await User.all({include: [{model: Role, as: 'role'}]}) as User[];
        return users.map(user => user.getUserWithoutPassword());
    }

    @Post('/')
    @HttpCode(201)
    @Authorized([DefaultRoles.ADMIN])
    public async createUser(@Body() data: CreateUser) {
        const user: User = await this.userService.createUser(data);
        return user.getUserWithoutPassword();
    }

    @Get('/:id')
    @Authorized([DefaultRoles.ADMIN])
    public async getById(@Param('id') id: number) {
        const user: User = await this.findUserById(id) as User;
        return user.getUserWithoutPassword();
    }

    @Patch('/:id')
    @Authorized([DefaultRoles.ADMIN])
    public async updateUser(@Param('id') id: number, @Body() data: UpdateUser) {
        let user: User = await this.findUserById(id);
        user = await this.userService.updateUser(user, data);
        return user.getUserWithoutPassword();
    }

    @Delete('/:id')
    @OnNull(200)
    @OnUndefined(200)
    @Authorized([DefaultRoles.ADMIN])
    public async deleteUser(@Param('id') id: number) {
        const user: User = await this.findUserById(id);
        const articles: Article[] = await (user as any).getArticles();
        for (const article of articles){
            await this.articleService.deleteArticle(article);
        }
        await user.destroy();
        return {};
    }

    @Post('/:id/change-password')
    @Authorized([DefaultRoles.ADMIN])
    public async changePassword(@Param('id') id: number, @Body() data: ChangePasswordByAdmin) {
        let user: User = await this.findUserById(id) as User;
        user = await this.userService.changePassword(user, data.newPassword);
        return user.getUserWithoutPassword();
    }


    async findUserById(id: number) {
        const user: User = await User.findUserById(id, {include: [{model: Role, as: 'role'}]}) as User;
        if (!user) {
            throw new ObjectDoesNotExistsError({detail: `User with id ${id} not found`});
        }
        return user;
    }
}