import * as path from 'path';

export interface AppConfig {
    secretKey: string;
    jwtTokenExpiresMinutes: number;
    jwtBearer: string;
    userAvatarPath: string;
    basePath: string;
}

export enum DefaultRoles {
    ADMIN = 'admin',
    PREMIUM = 'premium user',
    USER = 'user'
}

export enum DefaultRolesIds {
    ADMIN = 1,
    USER = 2,
    PREMIUM = 3,
}

export const appConfig: AppConfig = {
    secretKey: 'AMJ2RUd3hmGuliGlSYRMaUT8QmYnwJJD',
    jwtTokenExpiresMinutes: 60,
    jwtBearer: 'Bearer',
    userAvatarPath: 'media/users',
    basePath: path.join(__dirname, '../../'),
};