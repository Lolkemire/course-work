// import * as databaseConfig from './config.json';
// databaseConfig.default.deve

const dbHost = process.env.DB_HOST;

console.log(dbHost);

export interface IDatabaseConfig {
    database: string;
    username: string;
    password: string;
    dialect: string;
    host: string;
    port: number;
    logging: boolean;
    storage?: string;
}


export const devPostgres: IDatabaseConfig = {
    database: 'course-work-dev',
    username: 'postgres',
    password: 'postgres',
    dialect: 'postgres',
    host: dbHost,
    logging: true,
    port: 5432
};

export const prodPostgres: IDatabaseConfig = {
    database: 'course-work-prod',
    username: 'postgres',
    password: 'postgres',
    dialect: 'postgres',
    host: dbHost,
    logging: false,
    port: 5432
};

export const testPostgres: IDatabaseConfig = {
    database: 'course-work-test',
    username: 'postgres',
    password: 'postgres',
    dialect: 'postgres',
    host: dbHost,
    logging: false,
    port: 5432
};