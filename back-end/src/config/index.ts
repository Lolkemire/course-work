import { devPostgres, IDatabaseConfig, prodPostgres, testPostgres } from './databaseConfig';
import { AppConfig, appConfig } from './app';


class Configs {
    private _appConfig: AppConfig;
    private _databaseConfig: IDatabaseConfig;

    constructor() {
        this._appConfig = appConfig;
        switch (process.env.NODE_ENV) {
            case 'test':
                this._databaseConfig = testPostgres;
                break;
            case 'development':
                this._databaseConfig = devPostgres;
                break;
            case 'production':
                this._databaseConfig = prodPostgres;
                break;
            default:
                this._databaseConfig = devPostgres;
        }
    }

    public getAppConfig() {
        return this._appConfig;
    }

    public getDatabaseConfig() {
        return this._databaseConfig;
    }
}

export const configs = new Configs();