import {BaseError, BaseErrorMessageInterface} from './BaseError';


export class ForbiddenError extends BaseError {
    constructor(message?: BaseErrorMessageInterface) {
        super(403, 'ForbiddenError', message);
    }
}