import {BaseError, BaseErrorMessageInterface} from './BaseError';

export class ObjectDoesNotExistsError extends BaseError {
    constructor(message?: BaseErrorMessageInterface) {
        super(404, 'ObjectDoesNotExistsError', message);
    }
}