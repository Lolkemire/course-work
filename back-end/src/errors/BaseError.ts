import {HttpError} from 'routing-controllers';

export interface BaseErrorMessageInterface {
    detail?: string;
    errors?: {[key: string]: any};
}

export class BaseError extends HttpError {
    name: string;

    constructor(code: number, name: string, message?: BaseErrorMessageInterface) {
        super(code, message as any);
        this.name = name;
    }
}