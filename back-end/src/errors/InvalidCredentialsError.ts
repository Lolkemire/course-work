import {BaseError, BaseErrorMessageInterface} from './BaseError';


export class InvalidCredentialsError extends BaseError {
    constructor(message?: BaseErrorMessageInterface) {
        super(400, 'InvalidCredentialsError', message);
    }
}