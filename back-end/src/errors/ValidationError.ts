import { ValidationError as SequelizeValidationError } from 'sequelize';
import {BaseError, BaseErrorMessageInterface} from './BaseError';

const UNIQUE_VIOLATION = 'unique violation';

export class ValidationError extends BaseError {

    constructor(message?: BaseErrorMessageInterface) {
        super(400, 'ValidationError', message);
    }

    static buildExceptionFromModelError(validationError: SequelizeValidationError) {
        const generatedError: BaseErrorMessageInterface = {
            detail: 'failed',
            errors: {}
        };
        if (validationError && validationError.hasOwnProperty('errors')) {
            for (const error of validationError.errors){
                generatedError.errors[error.path] = error;
                if (error.type === UNIQUE_VIOLATION) {
                    generatedError.detail = UNIQUE_VIOLATION;
                }
            }
        }
        return new ValidationError(generatedError);
    }
}