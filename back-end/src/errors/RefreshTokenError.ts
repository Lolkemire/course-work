import {BaseError, BaseErrorMessageInterface} from './BaseError';


export class RefreshTokenError extends BaseError {
    constructor(message?: BaseErrorMessageInterface) {
        super(400, 'RefreshTokenError', message);
    }
}