import {IsEmpty, IsNotEmpty, MaxLength} from 'class-validator';

export class CreateComment {
    @IsNotEmpty()
    @MaxLength(255)
    text: string;

    @IsNotEmpty()
    article_id: number;

    author_id: number;
}