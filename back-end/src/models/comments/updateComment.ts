import {IsEmpty, IsNotEmpty, MaxLength} from 'class-validator';

export class UpdateComment {
    @IsNotEmpty()
    @MaxLength(255)
    text: string;


}