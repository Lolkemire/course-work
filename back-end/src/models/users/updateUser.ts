import { ValidateIf, IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export default class UpdateUser {
    username: string;

    @ValidateIf((o) => o.email)
    @IsEmail()
    email: string;

    role_id: number;

    firstName: string;

    lastName: string;

    createdAt: Date;

    updatedAt: Date;

}