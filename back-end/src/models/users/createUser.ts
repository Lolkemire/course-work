import { IsNotEmpty, IsEmail, MinLength, MaxLength,  } from 'class-validator';
export default class CreateUser {
    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(32)
    password: string;

    avatar: string;

    role_id: number;

    firstName: string;

    lastName: string;

}