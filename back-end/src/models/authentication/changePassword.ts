import { IsEqualTo } from '../validators/equal-to';
import { IsNotEmpty, MinLength, MaxLength } from 'class-validator';
export default class ChangePassword {
    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(32)
    oldPassword: string;

    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(32)
    newPassword: string;

    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(32)
    @IsEqualTo('newPassword', {message: 'Confrim password must be equal to new password.'})
    confirmNewPassword: string;
}