import { IsUserAlreadyExist } from '../validators/is-user-exists';
import { IsNotEmpty, IsEmail } from 'class-validator';

/**
 * Domain model which represents credentials in login operation
 *
 * @export
 * @class LoginCredentials
 */
export default class LoginCredentials {
    @IsNotEmpty()
    @IsEmail()
    // @IsUserAlreadyExist({message: 'User with that email doesn\'t exists.'})
    email: string;

    @IsNotEmpty()
    password: string;
}