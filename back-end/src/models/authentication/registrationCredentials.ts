import { IsEqualTo } from '../validators/equal-to';
import {IsNotEmpty, IsEmail, MinLength, MaxLength, Validator} from 'class-validator';


/**
 * Domain model which represents credentials in registration operation
 *
 * @export
 * @class RegistrationCredentials
 */
export default class RegistrationCredentials {
    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(32)
    password: string;

    @IsNotEmpty()
    @MinLength(6)
    @MaxLength(32)
    @IsEqualTo('password', {
        message: 'Confirm password must be equal to password.'
    })
    confirmPassword: string;
}