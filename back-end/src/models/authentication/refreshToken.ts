import { IsNotEmpty } from 'class-validator';
export default class RefreshToken {
    @IsNotEmpty()
    token: string;
}