import {IsNotEmpty} from 'class-validator';

export class UpdateArticle {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    text: string;
}