import { IsNotEmpty, Length } from 'class-validator';
export class CreateArticle {
    @IsNotEmpty()
    @Length(6, 32)
    title: string;

    @IsNotEmpty()
    text: string;

    author_id: number;

    image: string;
}