import {Action} from 'routing-controllers';
import {userFromHeader} from './userFromHeader';
import User from '../database/entities/User';

/**
 * Perform check user existing and is user role in array of allowed roles
 * @param {Action} action
 * @param {string[]} roles
 * @returns {Promise<boolean>}
 */
export async function authorizationChecker (action: Action, roles: string[]) {
    const user: User = await userFromHeader(action);
    if (!user) {
        return false;
    }
    if (user && !roles.length) {
        return true;
    }
    return !!(user && roles.find(role => role === user.role.name));
}
