import { Action } from 'routing-controllers';
import { userFromHeader } from './userFromHeader';

/**
 * Return current user from header
 * @param {Action} action
 * @returns {Promise<User>}
 */
export async function currentUserChecker(action: Action) {
    return userFromHeader(action);
}
