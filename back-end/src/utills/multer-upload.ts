import * as path from 'path';
import * as multer from 'multer';
import { Request } from 'express';

export const avatarUpload = {
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, `./media/users/`);
        },
        filename: (req, file, cb) => {
            const extension = path.extname(file.originalname);
            cb(null, `${new Date().getTime()}-avatar-${file.originalname}`);
        }
    })
};

export const articleImageUpload = {
    storage: multer.diskStorage({
        destination: (req: Request, file, cb) => {
            cb(null, `./media/temp/articles/thumbnails/`);
        },
        filename: (req, file, cb) => {
            const extension = path.extname(file.originalname);
            cb(null, `${new Date().getTime()}-thumbnail-${file.originalname}`);
        }
    })
};