import {Container} from 'typedi';
import {AuthenticationService} from '../services/authentication';
import {Action} from 'routing-controllers';
import User from '../database/entities/User';
import {configs} from '../config';

const authenticationService = Container.get(AuthenticationService);
const appConfig = configs.getAppConfig();

/**
 * Extract jwt token and validate bearer
 * @param {Action} action
 * @returns {string} jwt token
 */
export function extractToken(action: Action): string {
    const header: string = action.request.headers['authorization'];
    if (!header) {
        return null;
    }

    const [bearer, token] = header.split(' ');
    if (!token) {
        return null;
    }

    if (bearer !== appConfig.jwtBearer) {
        return null;
    }
    return token;
}

/**
 * Extract user from database by decoded token payload
 * @param {Action} action
 * @returns {Promise<User>}
 */
export async function userFromHeader(action: Action) {
    const token: string = extractToken(action);
    let payload: any = null;
    try {
        payload = authenticationService.verifyToken(token);
    } catch (e) {
        return null;
    }
    if (!payload) {
        return null;
    }
    return await User.findUserById(payload.id);
}