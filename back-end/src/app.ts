/**
 * Module dependencies.
 */
import './database';
import 'reflect-metadata';

import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';
import {createExpressServer, useContainer, useExpressServer} from 'routing-controllers';
import {Container} from 'typedi';

import {authorizationChecker} from './utills/authorizationChecker';
import {currentUserChecker} from './utills/currentUserChecker';
import {AdminUsersController} from './controllers/admin/users';
import {AuthenticationController} from './controllers/authentication';
import {UsersController} from './controllers/users';
import {HomeController} from './controllers/home';
import {RolesController} from './controllers/roles';
import {ArticlesController} from './controllers/articles';
import {CommentsController} from './controllers/comments';
import {Request, Response} from 'express';

useContainer(Container);
export const app = express();

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, '../', 'public'), {maxAge: 31557600000}));
app.use('/media', express.static(path.join(__dirname, '../', 'media'), {maxAge: 31557600000}));
// app.use(errorHandler());
useExpressServer(app, {
    currentUserChecker: currentUserChecker,
    authorizationChecker: authorizationChecker,
    controllers: [
        AdminUsersController,
        RolesController,
        UsersController,
        AuthenticationController,
        ArticlesController,
        CommentsController,
        // HomeController,
    ],
    middlewares: [__dirname + '/middlewares/*.ts'],
    defaultErrorHandler: true
});

app.get('*', (req: Request, res: Response) => {
    return res.render('home.pug');
});

// app.use(compression());
