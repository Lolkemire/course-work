import { configs } from '../config';
import { IDatabaseConfig } from '../config/databaseConfig';
import { Sequelize } from 'sequelize-typescript';
import * as path from 'path';

const dbConfig: IDatabaseConfig = configs.getDatabaseConfig();

export const sequelize = new Sequelize({
    name: dbConfig.database,
    dialect: dbConfig.dialect,
    username: dbConfig.username,
    password: dbConfig.password,
    logging: dbConfig.logging,
    host: dbConfig.host,
    modelPaths: [path.join(__dirname, 'entities')],
});