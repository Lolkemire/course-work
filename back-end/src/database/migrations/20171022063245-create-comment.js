'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('comments', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            author_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                unique: false,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            article_id: {
                type: Sequelize.INTEGER,
                unique: false,
                allowNull: false,
                references: {
                    model: 'articles',
                    key: 'id'
                }
            },
            text: {
                type: Sequelize.STRING(255),
                allowNull: false
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW,
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('comments');
    }
};
