'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('roles', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING(32),
                unique: true
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('roles');
    }
};