'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('users', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            username: {
                type: Sequelize.STRING(32),
                unique: true
            },
            email: {
                type: Sequelize.STRING(64),
                unique: true
            },
            password: {
                type: Sequelize.STRING(256)
            },
            firstName: {
                type: Sequelize.STRING(32),
                allowNull: true
            },
            lastName: {
                type: Sequelize.STRING(32),
                allowNull: true
            },
            avatar: {
                type: Sequelize.STRING(256),
                allowNull: true
            },
            role_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'roles',
                    key: 'id'
                }
            }
        })
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('users');
    }
};