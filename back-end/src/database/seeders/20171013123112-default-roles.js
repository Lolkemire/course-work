'use strict';
const defaultRoles = require('../../config/defaultRolesConfig');

module.exports = {
  up: (queryInterface, Sequelize) => {
    let roles = Object.keys(defaultRoles).map(role => defaultRoles[role]);
    return queryInterface.bulkInsert('roles', roles, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('roles', null, {});
  }
};
