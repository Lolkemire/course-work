'use strict';
const crypto = require('crypto');
const defaultRoles = require('../../config/defaultRolesConfig');
const secret = 'AMJ2RUd3hmGuliGlSYRMaUT8QmYnwJJD';


function passwordHash(raw) {
  const hash = crypto.createHmac('sha256', secret);
  return hash.update(raw).digest('hex');
}

// password = 123456789 = 7bfd793398e32bdc95a9ed6370e732f23982f4b595f5428ce68119ba9be064e7
const ADMIN_ROLE_ID = defaultRoles.ADMIN.id;
const USER_ROLE_ID = defaultRoles.USER.id;
const PREMIUM_USER_ROLE_ID = defaultRoles.PREMIUM.id;

const password = passwordHash('123456789');

const users = [
  {
    username: 'JohnDoe987',
    email: 'johndoe@example.com',
    password: password,
    role_id: USER_ROLE_ID,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    username: 'FCarey12',
    email: 'fcarey12@example.com',
    password: password,
    role_id: USER_ROLE_ID,
    firstName: 'Freddie',
    lastName: 'Carey',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    username: 'LinCoby',
    email: 'lcoby@example.com',
    password: password,
    role_id: USER_ROLE_ID,
    firstName: 'Lindsey',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    username: 'KekTobyMadisonKek',
    email: 'toby@example.com',
    role_id: PREMIUM_USER_ROLE_ID,
    password: password,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    username: 'evelyn555',
    email: 'evelyn555@example.com',
    password: password,
    role_id: ADMIN_ROLE_ID,
    firstName: 'Hilary',
    lastName: 'Evelyn',
    createdAt: new Date(),
    updatedAt: new Date(),
  },
];

// users = users.map((user) => {
//   return {
//     ...user,
//   }
// })

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('users', users)
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('users', users);
  }
};
