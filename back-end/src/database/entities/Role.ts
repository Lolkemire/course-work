import User from './User';
import {IsUUID, Column,  DataType,  HasMany,  Model,  PrimaryKey,  Table} from 'sequelize-typescript';

@Table({
    timestamps: false,
    tableName: 'roles',
})
export default class Role extends Model<Role> {

    @PrimaryKey
    @Column({
        autoIncrement: true,
    })
    id: number;

    @Column({type: DataType.STRING(32), unique: true})
    name: string;

    @HasMany(() => User)
    users: User[];
}