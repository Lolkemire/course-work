import {
    AllowNull,
    BelongsTo, Column, CreatedAt, DataType, ForeignKey, Model, PrimaryKey, Table,
    UpdatedAt
} from 'sequelize-typescript';
import User from './User';
import {Article} from './Article';

@Table({
    underscored: true,
    tableName: 'comments'
})
export class Comment extends Model<Comment> {
    @PrimaryKey
    @Column({
        autoIncrement: true,
    })
    id: number;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Column
    author_id: number;

    @BelongsTo(() => User, {onDelete: 'CASCADE'})
    author: User;

    @AllowNull(false)
    @ForeignKey(() => Article)
    @Column
    article_id: number;

    @BelongsTo(() => Article, {onDelete: 'CASCADE'})
    article: Article;

    @Column(DataType.STRING(255))
    text: string;

    @CreatedAt
    createdAt: Date;

    @UpdatedAt
    updatedAt: Date;

    public toPlain() {
        const plained = this.get({plain: true});
        if (this.author) {
            plained.author = this.author.getUserWithoutPassword();
        }
        return plained;
    }
}