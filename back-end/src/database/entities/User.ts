import Role from './Role';
import {
    BelongsTo, Column, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table, UpdatedAt, CreatedAt, IsEmail,
    IFindOptions, AllowNull,
} from 'sequelize-typescript';
import {Article} from './Article';
import {DefaultRoles} from '../../config/app';

@Table({
    underscored: true,
    tableName: 'users',
})
export default class User extends Model<User> {

    @PrimaryKey
    @Column({
        autoIncrement: true,
    })
    id: number;

    @Column({type: DataType.STRING(32), unique: true})
    username: string;

    @IsEmail
    @Column({type: DataType.STRING(64), unique: true})
    email: string;

    @Column({type: DataType.STRING(256)})
    password: string;

    @Column(DataType.STRING(32))
    firstName: string;

    @Column(DataType.STRING(32))
    lastName: string;

    @Column(DataType.STRING(256))
    avatar: string;

    @AllowNull(false)
    @ForeignKey(() => Role)
    @Column
    role_id: number;

    @BelongsTo(() => Role, {onDelete: 'CASCADE'})
    role: Role;

    @HasMany(() => Article, {onDelete: 'CASCADE'})
    articles: Article[];

    @CreatedAt
    createdAt: Date;

    @UpdatedAt
    updatedAt: Date;

    static async findUserById(identifier?: number | string, options?: IFindOptions): Promise<User | null> {
        options = options || {};
        options.include = [{model: Role, as: 'role'}];
        options.where = {id: identifier};
        return await super.findOne(options) as User;
    }


    public getUserWithoutPassword() {
        const plain = this.get({plain: true});
        delete plain['password'];
        return plain;
    }

    public get isAdmin() {
        return this.role && this.role.name === DefaultRoles.ADMIN;
    }

    public get isUser() {
        return this.role && this.role.name === DefaultRoles.USER;
    }

    public get isPremium() {
        return this.role && this.role.name === DefaultRoles.PREMIUM;
    }
}
