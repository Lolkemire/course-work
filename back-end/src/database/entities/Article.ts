import {
    AllowNull,
    BelongsTo, Column, CreatedAt, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table,
    UpdatedAt
} from 'sequelize-typescript';
import User from './User';
import {Comment} from './Comment';

@Table({
    underscored: true,
    tableName: 'articles',
})
export class Article extends Model<Article> {
    @PrimaryKey
    @Column({
        autoIncrement: true,
    })
    id: number;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Column
    author_id: number;

    @BelongsTo(() => User, {onDelete: 'CASCADE'})
    author: User;

    @Column(DataType.STRING(32))
    title: string;

    @Column(DataType.TEXT)
    text: string;

    @Column(DataType.STRING(256))
    image: string;

    @CreatedAt
    createdAt: Date;

    @UpdatedAt
    updatedAt: Date;

    @HasMany(() => Comment, {onDelete: 'CASCADE'})
    comments: Comment[];

    public toPlain() {
        const plained = this.get({plain: true});
        if (this.author) {
            plained.author = this.author.getUserWithoutPassword();
        }
        return plained;
    }

    toString(): string {
        return this.title;
    }
}