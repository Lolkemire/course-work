import {Service} from 'typedi';
import RegistrationCredentials from '../models/authentication/registrationCredentials';
import {PasswordHashService} from './passwordHash';
import LoginCredentials from '../models/authentication/loginCredentials';
import User from '../database/entities/User';
import {configs} from '../config';
import * as jwt from 'jsonwebtoken';
import RefreshToken from '../models/authentication/refreshToken';
import {UserService} from './user';
import Role from '../database/entities/Role';
import {InvalidCredentialsError} from '../errors/InvalidCredentialsError';
import {RefreshTokenError} from '../errors/RefreshTokenError';


const config = configs.getAppConfig();

/**
 * Service which provides methods of login and registration
 *
 * @export
 * @class AuthenticationService
 */
@Service()
export class AuthenticationService {
    constructor(protected passwordHash: PasswordHashService, protected userService: UserService) {

    }

    /**
     * Perform login system by email and password
     *
     * @param {LoginCredentials} credentials
     * @returns
     * @memberof AuthenticationService
     */
    public async loginByEmail(credentials: LoginCredentials) {
        const user = await User.findOne({
            where: {email: credentials.email},
            include: [{model: Role, as: 'role'}]
        }) as User;
        if (!user) {
            this.throwInvalidCredentialsException();
        }
        this.comparePasswords(user, credentials.password, true);
        const preparedUser = user.getUserWithoutPassword();
        const token = this.generateJWTToken(preparedUser);
        return {token, user: preparedUser};
    }

    /**
     * Perform registration in system by email and password
     *
     * @param {RegistrationCredentials} credentials user data
     * @return Token and created user
     * @memberof AuthenticationService
     */
    public async registrationByEmail(credentials: RegistrationCredentials) {
        const user = await this.createUser(credentials);
        const preparedUser = user.getUserWithoutPassword();
        const token = this.generateJWTToken(preparedUser);
        return {token, user: preparedUser};
    }

    /**
     * Perform user creation and catch error and rebuild to validation exeption
     *
     * @protected
     * @param {RegistrationCredentials} credentials registration credentials
     * @return {User} created user
     * @memberof AuthenticationService
     */
    protected async createUser(credentials: RegistrationCredentials) {
        return await this.userService.createUser({
            username: credentials.username,
            email: credentials.email,
            password: credentials.password,
        }) as User;
    }

    /**
     * Generate JWT token by user payload
     *
     * @param {Any} user user object
     * @return Generated token
     * @memberof AuthenticationService
     */
    public generateJWTToken(user: any) {
        const expiresIn = new Date();
        expiresIn.setMinutes(expiresIn.getMinutes() + config.jwtTokenExpiresMinutes);
        const exp = expiresIn.getTime() / 1000;
        return jwt.sign(user, config.secretKey, {
            expiresIn: exp.toString()
        });
    }

    /**
     * Perform token verification
     *
     * @protected
     * @param {string} token JWT Token
     * @return decoded payload
     * @memberof AuthenticationService
     */
    public verifyToken(token: string) {
        return jwt.verify(token, config.secretKey);
    }

    /**
     * Compare user password to raw password and throw exception if necessary
     *
     * @protected
     * @param {User} user User object
     * @param {string} password Raw password
     * @param {boolean} throwException Need throw exceptions
     * @return Result of comparing
     * @memberof AuthenticationService
     */
    protected comparePasswords(user: User, password: string, throwException: boolean = true) {
        const hashedPassword = this.passwordHash.generate(password);
        const isEqual = user.password === hashedPassword;
        if (!isEqual && throwException) {
            this.throwInvalidCredentialsException();
        }
        return isEqual;
    }

    /**
     * Throw validation exception with 400 code and message with
     * information about credentials error
     *
     * @protected
     * @memberof AuthenticationService
     */
    protected throwInvalidCredentialsException() {
        throw new InvalidCredentialsError({
            detail: 'Invalid email or password!'
        });
    }

    /**
     * Perform token refresh
     * @param {RefreshToken} token jwt token
     * @returns {Promise<{token: string; user: any}>}
     */
    public async refreshToken(token: RefreshToken): Promise<{token: string; user: any}> {
        let payload: any = null;
        try {
            payload = this.verifyToken(token.token);
        } catch (e) {
            throw new RefreshTokenError({errors: e.message});
        }
        const user = await User.findUserById(payload['id']) as User;
        const plainUser = user.getUserWithoutPassword();
        return {
            token: this.generateJWTToken(plainUser),
            user: plainUser
        };
    }
}
