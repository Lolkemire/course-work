import * as crypto from 'crypto';
import { configs } from '../config';
import { Service } from 'typedi';


const config = configs.getAppConfig();

/**
 * Service which containts password hashing logic
 *
 * @export
 * @class PasswordHashService
 */
@Service()
export class PasswordHashService {
    /**
     * Generate hash from raw password
     *
     * @param {string} rawPassword Raw password
     * @returns Hashed password
     * @memberof PasswordHashService
     */
    public generate(rawPassword: string) {
        const hash = crypto.createHmac('sha256', config.secretKey);
        return hash.update(rawPassword).digest('hex');
    }
}
