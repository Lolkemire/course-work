import * as fs from 'fs';
import * as path from 'path';
import {Service} from 'typedi';
import {CreateArticle} from '../models/articles/createArticle';
import {Article} from '../database/entities/Article';
import {Comment} from '../database/entities/Comment';
import {configs} from '../config/index';
import {ValidationError} from '../errors/ValidationError';
import User from '../database/entities/User';
import {UpdateArticle} from '../models/articles/updateArticle';

const appConfig = configs.getAppConfig();

@Service()
export class ArticlesService {

    /**
     * Perform article creation
     * @param {CreateArticle} data article data
     * @param image article thumbnail file
     * @returns {Promise<Article>}
     */
    public async createArticle(data: CreateArticle, image: any) {
        const oldPath = path.join(appConfig.basePath, image.path);
        data.image = image.path;
        let newArticle: Article = null;
        try {
            newArticle = await Article.create(data) as Article;
        } catch (e) {
            fs.unlinkSync(oldPath);
            throw ValidationError.buildExceptionFromModelError(e);
        }

        await this.updateArticleImage(newArticle, image);
        return newArticle;
    }

    /**
     * Move article file from temp folder to dynamic created folder by article id
     * @param {Article} article
     * @param image
     * @returns {Promise<string>}
     */
    protected async moveArticleImage(article: Article, image: any): Promise<string> {
        const oldPath = path.join(appConfig.basePath, image.path);
        const newPathDir = path.join(appConfig.basePath, `/media/articles/${article.id}/`);
        const newPath = path.join(newPathDir, image.originalname);
        if (!fs.existsSync(newPathDir)) {
            fs.mkdirSync(newPathDir);
        }
        fs.renameSync(oldPath, newPath);
        return newPath;
    }

    /**
     * Perform article thumbnail update, change old to new
     * @param {Article} article
     * @param image
     * @returns {Promise<Article>}
     */
    protected async updateArticleImage(article: Article, image: any) {
        const newPath = await this.moveArticleImage(article, image);
        article.image = newPath.replace(appConfig.basePath, '/');
        await article.save({});
        return article;
    }


    /**
     * Perform article update with thumbnail update
     * @param {Article} article
     * @param {UpdateArticle | any} data
     * @param image
     * @returns {Bluebird<Article>}
     */
    public async updateArticle(article: Article, data: UpdateArticle | any, image: any) {
        if (image) {
            const imgPath = await this.moveArticleImage(article, image);
            data.image = imgPath.replace(appConfig.basePath, '/');
        }
        return article.update(data);
    }

    /**
     * Perform article update with comments deleting
     * @param {Article} article
     * @returns {Promise<void>}
     */
    public async deleteArticle(article: Article) {
        if (!article) {
            return;
        }
        const pathToImage = path.join(appConfig.basePath, article.image);
        if (article.image && fs.existsSync(pathToImage)) {
            fs.unlinkSync(pathToImage);
            // fs.rmdir(path.dirname(pathToImage));
        }
        const comments = await (article as any).getComments() as Comment[];
        for (const comment of comments) {
            await comment.destroy();
        }
        await article.destroy();
    }
}