import * as path from 'path';
import {configs} from '../config/index';
import * as fs from 'fs';
import User from '../database/entities/User';
import {Service} from 'typedi';
import {PasswordHashService} from './passwordHash';
import CreateUser from '../models/users/createUser';
import {DefaultRoles} from '../config/app';
import Role from '../database/entities/Role';
import {ObjectDoesNotExistsError} from '../errors/ObjectDoesNotExistsError';
import {ValidationError} from '../errors/ValidationError';
import { ValidationError as SequelizeValidationError } from 'sequelize';
import {BadRequestError} from 'routing-controllers';

const appConfig = configs.getAppConfig();

@Service()
export class UserService {
    constructor(private passwordHash: PasswordHashService) {

    }

    /**
     * Create user with hash password
     * @param {CreateUser | any} data
     * @returns {Promise<User>}
     */
    public async createUser(data: CreateUser | any): Promise<User> {
        data = Object.create(data);
        data['password'] = this.passwordHash.generate(data.password);
        return await this.performCreateUser(data);
    }

    /**
     * Perform user updating
     * @param {User} user
     * @param {{[p: string]: any}} data
     * @returns {Promise<User>}
     */
    public async updateUser(user: User, data: {[key: string]: any}): Promise<User> {
        user = await this.performUpdate(user, data);
        return await user.reload();
    }

    /**
     * Perform password change, validate old passwords if needs
     * @param {User} user
     * @param {string} newPassword
     * @param {string} oldPassword
     * @returns {Promise<User>}
     */
    public async changePassword(user: User, newPassword: string, oldPassword: string = '') {
        if (oldPassword) {
            this.compareOldPassword(user, oldPassword);
        }
        return this.performPasswordChanging(user, newPassword);
    }

    /**
     * Updating avatar with old deleting
     * @param {User} user
     * @param avatarFile
     * @returns {Promise<User>}
     */
    public async updateAvatar(user: User, avatarFile: any) {
        this.removeOldAvatar(user);
        return await this.updateUser(user, { avatar: avatarFile.path });
    }

    /**
     * Validate entered role, if not exists set default one (User role)
     * @param {{[p: string]: any}} userData
     * @returns {Promise<number>}
     */
    protected async validateRole(userData: { [key: string]: any }) {
        let role: Role = null;
        if (userData.role_id) {
            role = await Role.findById(userData.role_id) as Role;
        } else {
            role = await Role.findOne({where: { name: DefaultRoles.USER}}) as Role;
        }
        if (role) {
            return role.id;
        }
        throw new ObjectDoesNotExistsError({ detail: `Role ${userData} doesn't exists!` });
    }

    /**
     * Validate role, perform creation, catch and build error
     * @param {{[p: string]: any}} userData
     * @returns {Promise<any>}
     */
    protected async performCreateUser(userData: { [key: string]: any }) {
        userData.role_id = await this.validateRole(userData);
        try {
            return await User.create(userData, { include: [{ model: Role, as: 'role' }] }) as User;
        } catch (e) {
            throw ValidationError.buildExceptionFromModelError(e as any);
        }
    }

    /**
     * Perform password change throught user updating
     * @param {User} user
     * @param {string} newPassword
     * @returns {Promise<User>}
     */
    protected async performPasswordChanging(user: User, newPassword: string) {
        await this.updateUser(user, { password: newPassword });
        return user;
    }

    /**
     * Compare entered old password to password in database
     * @param {User} user
     * @param {string} oldPassword
     */
    protected compareOldPassword(user: User, oldPassword: string) {
        const hashedOldPassword = this.passwordHash.generate(oldPassword);
        if (user.password !== hashedOldPassword) {
            throw new ValidationError({ detail: 'Old password invalid!' });
        }
    }

    /**
     * Perform removing old avatar if exists
     * @param {User} user
     */
    protected removeOldAvatar(user: User) {
        if (user.avatar) {
            const avatarPath = path.join(appConfig.basePath, user.avatar);
            if (fs.existsSync(avatarPath)) {
                fs.unlinkSync(avatarPath);
            }
        }
    }

    /**
     * Perform user updating with password hashing, catch validation error and build own error
     * @param {User} user
     * @param data
     * @returns {Promise<User>}
     */
    protected async performUpdate(user: User, data: any) {
        if (user === null) {
            throw new ObjectDoesNotExistsError({detail: 'User does not exists' });
        }
        this.preaparePasswordToUpdate(data);
        try {
            return await user.update(data);
        } catch (e) {
            throw ValidationError.buildExceptionFromModelError(e);
        }
    }

    /**
     * Perform password hashing
     * @param {User} user
     * @param data
     * @returns {Promise<void>}
     */
    protected async preaparePasswordToUpdate(data: any) {
        if (data.hasOwnProperty('password')) {
            data['password'] = this.passwordHash.generate(data['password']);
        }
    }

}
