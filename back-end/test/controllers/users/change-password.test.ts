import * as supertest from 'supertest';
import {app} from '../../../src/app';
import User from '../../../src/database/entities/User';
import {PasswordHashService} from '../../../src/services/passwordHash';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {Container} from 'typedi';
import {UserService} from '../../../src/services/user';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const passwordHash = Container.get(PasswordHashService);
const userService = Container.get(UserService);

describe('Users api tests', () => {
    const userOne: any = {
        username: 'changePasswordTest',
        email: 'changePasswordTest@test.com',
        password: 'testtest',
    };
    let instancesOfuserOne: any = null;

    test('it SHOULD change password', async () => {
        const passwordUpdate = {
            oldPassword: userOne.password,
            newPassword: 'brandnewpassword',
            confirmNewPassword: 'brandnewpassword'
        };
        const path = `/api/v1/users/${instancesOfuserOne.id}/change-password`;
        const response = await authorizeRequest(request.post(path).send(passwordUpdate), userOne);
        expect(response.status).toBe(200);
        const user = await User.findOne({where: { id: instancesOfuserOne.id }}) as User;
        const hashedPassword = passwordHash.generate('brandnewpassword');
        expect(user.password).toBe(hashedPassword);
    });


    beforeAll(async () => {
        instancesOfuserOne = await createUser(userOne);
    });

    afterAll(async () => {
        await User.destroy({where: {username: userOne.username}});
    });

});
