import * as supertest from 'supertest';
import {app} from '../../../src/app';
import User from '../../../src/database/entities/User';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {Container} from 'typedi';
import {PasswordHashService} from '../../../src/services/passwordHash';
import {UserService} from '../../../src/services/user';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const passwordHash = Container.get(PasswordHashService);
const userService = Container.get(UserService);


describe('GET specified user api tests', () => {
    const userOne: any = {
        username: 'readUserTest',
        email: 'rut@test.com',
        password: 'testtest',
    };
    let instancesOfuserOne: any = null;


    test('it SHOULD return user with specified id', async () => {
        const path = '/api/v1/users/' + instancesOfuserOne['id'];
        const response = await authorizeRequest(request.get(path), userOne);
        expect(response.status).toBe(200);
        expect(response.body['username']).toBe(instancesOfuserOne['username']);
        expect(response.body['email']).toBe(instancesOfuserOne['email']);
        expect(response.body['id']).toBe(instancesOfuserOne['id']);
        expect(response.body['firstName']).toBe(instancesOfuserOne['firstName']);
        expect(response.body['lastName']).toBe(instancesOfuserOne['lastName']);
    });



    beforeAll(async () => {
        instancesOfuserOne = await createUser(userOne) as User;
    });

    afterAll(async () => {
        await User.destroy({ where: { username: userOne.username }});
    });
});
