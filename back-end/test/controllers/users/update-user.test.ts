import * as supertest from 'supertest';
import {app} from '../../../src/app';
import User from '../../../src/database/entities/User';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {Container} from 'typedi';
import {PasswordHashService} from '../../../src/services/passwordHash';
import {DefaultRolesIds} from '../../../src/config/app';
import {UserService} from '../../../src/services/user';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const passwordHash = Container.get(PasswordHashService);
const userService = Container.get(UserService);


describe('PATCH update user tests', () => {
    const userOne: any = {
        username: 'updateUserTest',
        email: 'updateUserTest@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.PREMIUM
    };
    const userToUpdate: any = {
        username: 'updateUserTestAnotherUser',
        email: 'updateUserTestAnotherUser@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.PREMIUM
    };
    const adminUserToUpdate: any = {
        username: 'adminUserToUpdate',
        email: 'adminUserToUpdate@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN
    };

    let instanceOfUserToUpdate: User = null;
    let instanceOfAdminUserToUpdate: User = null;


    test('it SHOULD update user', async () => {
        const updateData = {
            firstName: 'suppaTest',
            lastName: 'Kek',
        };
        const path = '/api/v1/users/' + instanceOfUserToUpdate['id'];
        const response = await authorizeRequest(request.patch(path).send(updateData), userOne);
        expect(response.status).toBe(200);
        expect(response.body['firstName']).toBe(updateData.firstName);
        expect(response.body['lastName']).toBe(updateData.lastName);
    });

    test('it SHOULD\'T update user because target user is admin', async () => {
        const updateData = {
            firstName: 'suppaTest',
            lastName: 'Kek',
        };
        const path = '/api/v1/users/' + instanceOfAdminUserToUpdate['id'];
        const response = await authorizeRequest(request.patch(path).send(updateData), userOne);
        expect(response.status).toBe(403);
    });


    beforeAll(async () => {
        await createUser(userOne);
        instanceOfAdminUserToUpdate = await createUser(adminUserToUpdate) as User;
        instanceOfUserToUpdate = await createUser(userToUpdate) as User;
    });

    afterAll(async () => {
        await User.destroy({ where: { username: userOne.username } });
        await User.destroy({ where: { username: adminUserToUpdate.username} });
        await User.destroy({ where: { username: userToUpdate.username} });
    });
});
