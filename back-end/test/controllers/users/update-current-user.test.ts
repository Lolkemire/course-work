import * as supertest from 'supertest';
import {Container} from 'typedi';

import {app} from '../../../src/app';
import {PasswordHashService} from '../../../src/services/passwordHash';
import {authorizeRequest} from '../../utills/authorizeRequest';
import User from '../../../src/database/entities/User';
import {UserService} from '../../../src/services/user';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const passwordHash = Container.get(PasswordHashService);
const userService = Container.get(UserService);


describe('PATCH update current user api tests', () => {
    const userOne: any = {
        username: 'UpdateUserOne',
        email: 'uuo@test.com',
        password: 'testtest',
    };
    let userInstance: User = null;


    test('it SHOULD update user', async () => {
        const updateData = {
            firstName: 'userBySelf',
            lastName: 'test',
        };
        const path = `/api/v1/users/${userInstance.id}`;
        const response = await authorizeRequest(request
            .patch(path).send(updateData), userOne);
        expect(response.status).toBe(200);
        expect(response.body['firstName']).toBe(updateData.firstName);
        expect(response.body['lastName']).toBe(updateData.lastName);
    });


    beforeAll(async () => {
        userInstance = await createUser(userOne);
    });

    afterAll(async () => {
        await User.destroy( {where: { username: userOne.username }});
    });


});
