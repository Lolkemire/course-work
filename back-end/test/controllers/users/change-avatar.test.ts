import * as fs from 'fs';
import * as supertest from 'supertest';
import * as path from 'path';
import {app} from '../../../src/app';
import User from '../../../src/database/entities/User';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {UserService} from '../../../src/services/user';
import {Container} from 'typedi';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);


describe('Users api tests', () => {
    const userOne: any = {
        username: 'changeAvatarTest9',
        email: 'changeAvatarTest9@test.com',
        password: 'testtest',
    };
    let userInstance: User = null;

    beforeAll(async () => {
        userInstance = await createUser(userOne);
    });

    afterAll(async () => {
        const user = await User.findOne({where: {username: userOne.username}}) as User;
        const pathToFile = path.join(__dirname, '../../../', user.avatar);
        fs.unlink(pathToFile);
        await User.destroy({where: {username: userOne.username}});
    });

    test('it SHOULD update avatar', async () => {
        const endpointPath = `/api/v1/users/${userInstance.id}/change-avatar`;
        const filePath = path.resolve(__dirname, '../../', 'doge.jpg');
        const response = await authorizeRequest(request.post(endpointPath)
            .attach('avatar', filePath), userOne);
        expect(response.status).toBe(200);
        const user = await User.findOne({where: {username: userOne.username}}) as User;
        expect(response.body['avatar']).toBe(user.avatar);
    });


});
