import * as supertest from 'supertest';
import {app} from '../../../src/app';
import User from '../../../src/database/entities/User';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {UserService} from '../../../src/services/user';
import {Container} from 'typedi';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);


describe('GET current user api tests', () => {
    const userOne: any = {
        username: 'currentUserTests',
        email: 'currentUserTests@test.com',
        password: 'testtest',
    };
    let instancesOfuserOne: any = null;


    test('it SHOULD return current authenticated user', async () => {

        const response = await authorizeRequest(request
            .get('/api/v1/users/current'), userOne);
        expect(response.status).toBe(200);
        expect(response.body['username']).toBe(userOne.username);
    });

    test('it SHOULD\' return, unauthenticated', async () => {
        const response = await request
            .get('/api/v1/users/current');
        expect(response.status).toBe(403);
    });

    beforeAll(async () => {
        instancesOfuserOne = await createUser(userOne);
    });

    afterAll(async () => {
        await User.destroy({where: {username: userOne.username}});
    });
});
