import User from '../../../src/database/entities/User';
import * as supertest from 'supertest';
import {app} from '../../../src/app';
import {Container} from 'typedi';
import {AuthenticationService} from '../../../src/services/authentication';
import {UserService} from '../../../src/services/user';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);


describe('Refresh token test', () => {
    const authenticationService = Container.get(AuthenticationService);

    const refreshUser: any = {
        username: 'refreshToken',
        email: 'refresh@test.com',
        password: 'testtest',
    };
    test('it SHOULD refresh token', async () => {
        const loginResult = await authenticationService.loginByEmail({
            email: refreshUser.email,
            password: refreshUser.password,
        });
        const response = await request.post('/api/v1/auth/refresh-token')
        .send({token: loginResult.token});
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('token');
        expect(response.body).toHaveProperty('user');
        expect(response.body['user'].id).toBe(loginResult.user.id);
    });
    beforeAll(async () => {
        await createUser(refreshUser);
    });
    afterAll(async () => {
        await User.destroy({where: { username: refreshUser.username }});
    });
});