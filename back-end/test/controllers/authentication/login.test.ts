import User from '../../../src/database/entities/User';
import * as supertest from 'supertest';
import {app} from '../../../src/app';
import {UserService} from '../../../src/services/user';
import {Container} from 'typedi';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);

describe('Login api test', () => {

    test('it SHOULD login user', async () => {
        const loginCredentials = {
            email: 'logintest@gmail.com',
            password: 'testpassword',
        };
        const response = await request.post('/api/v1/auth/login').send(loginCredentials);
        expect(response.status).toBe(200);
    });

    test('it SHOULD\'t login user because login invalid', async () => {
        const loginCredentials = {
            login: 'asdas@gmail.com',
            password: 'testpassword',
        };
        const response = await request.post('/api/v1/auth/login').send(loginCredentials);
        expect(response.status).toBe(400);
    });

    beforeAll(async () => {
        const testLoginUser: any = {
            username: 'logintest',
            email: 'logintest@gmail.com',
            password: 'testpassword',
        };
        await createUser(testLoginUser);
    });

    afterAll(async () => {
        await User.destroy({where: { username: 'logintest' }});
    });
});