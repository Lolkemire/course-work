import User from '../../../src/database/entities/User';
import * as supertest from 'supertest';
import {app} from '../../../src/app';
import {UserService} from '../../../src/services/user';
import {Container} from 'typedi';
import {createUser} from '../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);


describe('Registration api tests', () => {

    test('it SHOULD reject because passwords and confirm not equal', async () => {
        const userData: any = {
            username: 'test',
            email: 'test@gmail.com',
            password: 'testpassword',
            confirmPassword: 'testpasswordinvalid',
        };
        const response = await request.post('/api/v1/auth/registration')
            .send(userData);
        expect(response.status).toBe(400);
        expect(response.body['name']).toBe('BadRequestError');
    });
    test('it SHOULD reject because password is missing', async () => {
        const userData: any = {
            username: 'test',
            email: 'test@gmail.com',
            confirmPassword: 'testpasswordinvalid',
        };
        const response = await request.post('/api/v1/auth/registration').send(userData);
        expect(response.status).toBe(400);
        expect(response.body['name']).toBe('BadRequestError');
    });
    test('it SHOULD reject because confirm passwords is missing', async () => {
        const userData: any = {
            username: 'test',
            email: 'test@gmail.com',
            password: 'testpassword',
        };
        const response = await request.post('/api/v1/auth/registration').send(userData);
        expect(response.status).toBe(400);
        expect(response.body['name']).toBe('BadRequestError');
    });

    test('it SHOULD create user', async () => {
        const userData: any = {
            username: 'test',
            email: 'testcreate@gmail.com',
            password: 'testpassword',
            confirmPassword: 'testpassword',
        };
        const response = await request.post('/api/v1/auth/registration').send(userData);
        expect(response.status).toBe(201);
        expect(response.body['user']['username']).toBe(userData.username);
        expect(response.body['user']['email']).toBe(userData.email);
    });

    test('it SHOULD reject because user already exists', async () => {
        const userData: any = {
            username: 'duplicate',
            email: 'test@gmail.com',
            password: 'testpassword',
            confirmPassword: 'testpassword'
        };
        const response = await request.post('/api/v1/auth/registration').send(userData);
        expect(response.status).toBe(400);
    });

    beforeAll(async () => {
        const userData: any = {
            username: 'duplicate',
            email: 'test@gmail.com',
            password: 'testpassword',
        };
        await createUser(userData);
    });

    afterAll(async () => {
        await User.destroy({where: { username: 'test' }});
        await User.destroy({where: { username: 'duplicate' }});
    });
});