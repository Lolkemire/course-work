import {DefaultRolesIds} from '../../../../src/config/app';
import * as supertest from 'supertest';
import {app} from '../../../../src/app';
import {authorizeRequest} from '../../../utills/authorizeRequest';
import {Container} from 'typedi';
import User from '../../../../src/database/entities/User';
import {PasswordHashService} from '../../../../src/services/passwordHash';
import {UserService} from '../../../../src/services/user';
import {createUser} from '../../../utills/createUser';
import {Article} from '../../../../src/database/entities/Article';

const request = supertest(app);
const passwordHash = Container.get(PasswordHashService);
const userService = Container.get(UserService);


describe('Users api tests', () => {
    const adminOne: any = {
        username: 'deleteUserTest',
        email: 'deleteUserTest@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN,
    };
    let instanceOfAdmin: User = null;

    const userWithoutArticles: any = {
        username: 'userWithoutArticles',
        email: 'userWithoutArticles@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.USER,
    };
    let instanceOfUserWithoutArticles: User = null;

    const userWithArticles: any  = {
        username: 'userWithArticles',
        email: 'userWithArticles@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.USER,
    };

    let instanceOfUserWithArticles: User = null;

    test('it SHOULD delete user', async () => {
        const urlPath = '/api/v1/admin/users/' + instanceOfUserWithoutArticles.id;
        const response = await authorizeRequest(request
            .delete(urlPath), adminOne);
        expect(response.status).toBe(200);
    });

    test('it SHOULD delete user with articles', async () => {
        const urlPath = '/api/v1/admin/users/' + instanceOfUserWithArticles.id;
        const response = await authorizeRequest(request
            .delete(urlPath), adminOne);
        expect(response.status).toBe(200);
    });

    beforeAll(async () => {
        instanceOfAdmin = await createUser(adminOne) as User;
        instanceOfUserWithoutArticles = await createUser(userWithoutArticles) as User;
        instanceOfUserWithArticles = await createUser(userWithArticles) as User;
        await Article.create({
            author_id: instanceOfUserWithArticles.id,
            title: 'kek',
            text: 'kuk',
            image: ''
        });
        await Article.create({
            author_id: instanceOfUserWithArticles.id,
            title: 'kek',
            text: 'kuk',
            image: ''
        });
        await Article.create({
            author_id: instanceOfUserWithArticles.id,
            title: 'kek',
            text: 'kuk',
            image: ''
        });
        await Article.create({
            author_id: instanceOfUserWithArticles.id,
            title: 'kek',
            text: 'kuk',
            image: ''
        });
    });

    afterAll(async () => {
        await User.destroy({where: { username: adminOne.username }});
        await User.destroy({where: { username: userWithArticles.username }});
        await User.destroy({where: { username: userWithoutArticles.username }});

    });
});
