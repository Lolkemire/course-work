import {DefaultRolesIds} from '../../../../src/config/app';
import * as supertest from 'supertest';
import {app} from '../../../../src/app';
import {authorizeRequest} from '../../../utills/authorizeRequest';
import User from '../../../../src/database/entities/User';
import {UserService} from '../../../../src/services/user';
import {Container} from 'typedi';
import {createUser} from '../../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);


describe('Users api tests', () => {
    const userOne: any = {
        username: 'createUserTestAdmin',
        email: 'createUserTestAdmin@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN,
    };
    let instancesOfuserOne: any = null;
    const createUserTest: any = {
        username: 'createUserTest',
        email: 'createUserTest@test.com',
        password: 'testtest',
        firstName: 'NAMEFIRST',
        lastName: 'NAMELAST',
    };

    test('it SHOULD create new user', async () => {
        const response = await authorizeRequest(request
            .post('/api/v1/admin/users')
            .send(createUserTest), userOne);
        expect(response.status).toBe(201);
        expect(response.body['username']).toBe(createUserTest.username);
        expect(response.body['email']).toBe(createUserTest.email);
        expect(response.body['firstName']).toBe(createUserTest.firstName);
        expect(response.body['lastName']).toBe(createUserTest.lastName);
    });

    test('it SHOULDN\'T create new user because already exists', async () => {
        const response = await authorizeRequest(request
            .post('/api/v1/admin/users')
            .send(createUserTest), userOne);
        expect(response.status).toBe(400);
        expect(response.body['message']['detail']).toBe('unique violation');
        expect(response.body['message']['errors']).toHaveProperty('username');
    });

    beforeAll(async () => {
        instancesOfuserOne = await createUser(userOne);
    });

    afterAll(async () => {
        await User.destroy({where: { username: userOne.username }});
        await User.destroy({where: { username: createUserTest.username }});

    });
});
