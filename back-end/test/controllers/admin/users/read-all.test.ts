import {DefaultRolesIds} from '../../../../src/config/app';
import * as supertest from 'supertest';
import {app} from '../../../../src/app';
import {authorizeRequest} from '../../../utills/authorizeRequest';
import User from '../../../../src/database/entities/User';
import {Container} from 'typedi';
import {UserService} from '../../../../src/services/user';
import {createUser} from '../../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);


describe('Users api tests', () => {
    const userOne: any = {
        username: 'readAllUsers',
        email: 'readAllUsers@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN,
    };


    test('it SHOULD return list of users', async () => {
        const usersCount = await User.count();
        const response = await authorizeRequest(request
            .get('/api/v1/admin/users'), userOne);
        expect(response.status).toBe(200);
        expect(response.body.length).toBe(usersCount);
    });

    beforeAll(async () => {
        await createUser(userOne);
    });

    afterAll(async () => {
        await User.destroy({where: { username: userOne.username }});
    });
});
