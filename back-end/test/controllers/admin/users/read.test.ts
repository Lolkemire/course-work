import {DefaultRolesIds} from '../../../../src/config/app';
import * as supertest from 'supertest';
import {app} from '../../../../src/app';
import {authorizeRequest} from '../../../utills/authorizeRequest';
import User from '../../../../src/database/entities/User';
import {UserService} from '../../../../src/services/user';
import {Container} from 'typedi';
import {createUser} from '../../../utills/createUser';

const request = supertest(app);
const userService = Container.get(UserService);


describe('Users api tests', () => {
    const userOne: any = {
        username: 'readTestUserAdmin',
        email: 'readTestUserAdmin@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN,
    };
    const userTwoPremium: any = {
        username: 'readTestUsersPremium',
        email: 'readTestUsersPremium@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.PREMIUM,
    };
    let instanceOfPremiumUser: User = null;

    test('it SHOULD return user by id', async () => {
        const path = `/api/v1/admin/users/${instanceOfPremiumUser.id}`;
        const response = await authorizeRequest(request
            .get(path), userOne);
        expect(response.status).toBe(200);
        expect(response.body['id']).toBe(instanceOfPremiumUser.id);
        expect(response.body['email']).toBe(instanceOfPremiumUser.email);
        expect(response.body['username']).toBe(instanceOfPremiumUser.username);
        expect(response.body['firstName']).toBe(instanceOfPremiumUser.firstName);
        expect(response.body['lastName']).toBe(instanceOfPremiumUser.lastName);
    });

    test('it SHOULD return 404 error', async () => {
        const path = `/api/v1/admin/users/9999999999`;
        const response = await authorizeRequest(request
            .get(path), userOne);
        expect(response.status).toBe(404);
    });

    beforeAll(async () => {
        await createUser(userOne);
        instanceOfPremiumUser = await createUser(userTwoPremium) as User;
    });

    afterAll(async () => {
        await User.destroy({where: { username: userOne.username }});
        await User.destroy({where: { username: userTwoPremium.username }});
    });
});
