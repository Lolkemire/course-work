import {DefaultRolesIds} from '../../../../src/config/app';
import * as supertest from 'supertest';
import {app} from '../../../../src/app';
import {authorizeRequest} from '../../../utills/authorizeRequest';
import {Container} from 'typedi';
import User from '../../../../src/database/entities/User';
import {PasswordHashService} from '../../../../src/services/passwordHash';
import {UserService} from '../../../../src/services/user';
import {createUser} from '../../../utills/createUser';

const request = supertest(app);
const passwordHash = Container.get(PasswordHashService);
const userService = Container.get(UserService);


describe('Admin users api tests', () => {
    const userOne: any = {
        username: 'changePasswordAdminTest',
        email: 'changePasswordAdminTest@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN,
    };
    let instancesOfuserOne: User = null;

    test('it SHOULD change password', async () => {
        const passwordChangeData = {
            newPassword: 'testtest',
            confirmNewPassword: 'testtest',
        };
        const urlPath = `/api/v1/admin/users/${instancesOfuserOne.id}/change-password`;
        const response = await authorizeRequest(request
            .post(urlPath)
            .send(passwordChangeData), userOne);
        const user = await User.findUserById(instancesOfuserOne.id) as User;
        const hashedPassword = passwordHash.generate(passwordChangeData.newPassword);
        expect(response.status).toBe(200);
        expect(user.password).toBe(hashedPassword);
    });

    beforeAll(async () => {
        instancesOfuserOne = await createUser(userOne ) as User;
    });

    afterAll(async () => {
        await User.destroy({where: { username: userOne.username }});

    });
});
