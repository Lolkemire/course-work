import {DefaultRolesIds, DefaultRoles} from '../../../../src/config/app';
import * as supertest from 'supertest';
import {app} from '../../../../src/app';
import {authorizeRequest} from '../../../utills/authorizeRequest';
import {Container} from 'typedi';
import User from '../../../../src/database/entities/User';
import {PasswordHashService} from '../../../../src/services/passwordHash';
import {UserService} from '../../../../src/services/user';
import {createUser} from '../../../utills/createUser';

const request = supertest(app);
const passwordHash = Container.get(PasswordHashService);
const userService = Container.get(UserService);


describe('Users api tests', () => {
    const userOne: any = {
        username: 'updateUserTestAdmin',
        email: 'updateUserTestAdmin@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN,
    };
    let instancesOfuserOne: User = null;

    test('it SHOULD update user', async () => {
        const updateData = {
            firstName: 'Kek',
            lastName: 'kuk',
            email: 'brandNewEmail@test.com',
            role_id: DefaultRolesIds.USER,
        };
        const urlPath = '/api/v1/admin/users/' + instancesOfuserOne.id;
        const response = await authorizeRequest(request
            .patch(urlPath).send(updateData), userOne);
        expect(response.status).toBe(200);
        expect(response.body['email']).toBe(updateData.email);
        expect(response.body['firstName']).toBe(updateData.firstName);
        expect(response.body['lastName']).toBe(updateData.lastName);
        expect(response.body['role_id']).toBe(updateData.role_id);
        expect(response.body['role']['name']).toBe(DefaultRoles.USER);
    });

    beforeAll(async () => {
        instancesOfuserOne = await createUser(userOne) as User;
    });

    afterAll(async () => {
        await User.destroy({where: { username: userOne.username }});
    });
});
