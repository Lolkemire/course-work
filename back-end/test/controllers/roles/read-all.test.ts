import Role from '../../../src/database/entities/Role';
import {app} from '../../../src/app';
import * as supertest from 'supertest';

const request = supertest(app);


describe('Roles api tests', () => {

    test('it SHOULD return list of roles', async () => {
        const usersCount = await Role.count();
        const response = await request
            .get('/api/v1/roles');
        expect(response.status).toBe(200);
        expect(response.body.length).toBe(usersCount);
    });


});
