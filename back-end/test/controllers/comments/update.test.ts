import * as supertest from 'supertest';
import {app} from '../../../src/app';
import {configs} from '../../../src/config/index';
import {DefaultRolesIds} from '../../../src/config/app';
import {Container} from 'typedi';
import User from '../../../src/database/entities/User';
import {Article} from '../../../src/database/entities/Article';
import {createUser} from '../../utills/createUser';
import {ArticlesService} from '../../../src/services/articles';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {Comment} from '../../../src/database/entities/Comment';


const request = supertest(app);
const appConfig = configs.getAppConfig();
const articlesService = Container.get(ArticlesService);

describe('Comments api tests', () => {
    const userOne: any = {
        username: 'delcom99UserOne',
        email: 'delcom99UserOne@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.USER
    };
    const adminTwo: any = {
        username: 'delcomUserTwo',
        email: 'delcomUserTwo@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN
    };
    const premiumThree: any = {
        username: 'delcomUserThree',
        email: 'delcomUserThree@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.PREMIUM
    };
    let instanceOfUserOne: User = null;
    let instanceOfAdminTwo: User = null;
    let instanceOfPremiumThree: User = null;

    let articleOne: Article = null;
    let articleTwo: Article = null;

    let instanceOfCommentOne: Comment = null;
    let instanceOfCommentTwo: Comment = null;

    test('User with role "user" cannot update comments', async () => {
        const dataToUpdate = {
            text: 'TopKek'
        };
        const pathToComment = `/api/v1/comments/${instanceOfCommentOne.id}`;
        const response = await authorizeRequest(
            request.patch(pathToComment).send(dataToUpdate),
            userOne
        );
        expect(response.status).toBe(403);
    });
    test('User with role "premium user" can update own comments', async () => {
        const dataToUpdate = {
            text: 'TopKek'
        };
        const pathToComment = `/api/v1/comments/${instanceOfCommentTwo.id}`;
        const response = await authorizeRequest(
            request.patch(pathToComment).send(dataToUpdate),
            premiumThree
        );
        expect(response.status).toBe(200);
        expect(response.body['text']).toBe(dataToUpdate.text);
    });

    test('User with role "premium user" cannot update comments of another users', async () => {
        const dataToUpdate = {
            text: 'TopKek'
        };
        const pathToComment = `/api/v1/comments/${instanceOfCommentOne.id}`;
        const response = await authorizeRequest(
            request.patch(pathToComment).send(dataToUpdate),
            premiumThree
        );
        expect(response.status).toBe(403);
    });

    test('User with role "admin" can update comments', async () => {
        const dataToUpdate = {
            text: 'TopKek'
        };
        const pathToComment = `/api/v1/comments/${instanceOfCommentOne.id}`;
        const response = await authorizeRequest(
            request.patch(pathToComment).send(dataToUpdate),
            adminTwo
        );
        expect(response.status).toBe(200);
        expect(response.body['text']).toBe(dataToUpdate.text);
    });


    beforeAll(async () => {
        instanceOfUserOne = await createUser(userOne) as User;
        instanceOfAdminTwo = await createUser(adminTwo) as User;
        instanceOfPremiumThree = await createUser(premiumThree) as User;
        articleOne = await Article.create({
            title: 'ArticleOne',
            text: 'Kek mek',
            author_id: instanceOfUserOne.id,
            image: ''
        }) as Article;
        articleTwo = await Article.create({
            title: 'ArticleOne',
            text: 'Kek mek',
            author_id: instanceOfPremiumThree.id,
            image: ''
        }) as Article;
        const commentOne = {
            text: 'commentOne',
            article_id: articleOne.id,
            author_id: instanceOfUserOne.id
        };

        const commentTwo = {
            text: 'commentOne',
            article_id: articleTwo.id,
            author_id: instanceOfPremiumThree.id
        };
        instanceOfCommentOne = await Comment.create(commentOne) as Comment;
        instanceOfCommentTwo = await Comment.create(commentTwo) as Comment;
    });

    afterAll(async () => {
        const comments = await Comment.all();
        for (const comment of comments) {
            await comment.destroy();
        }
        await articlesService.deleteArticle(articleOne);
        await articlesService.deleteArticle(articleTwo);
        await User.destroy({where: {username: userOne.username}});
        await User.destroy({where: {username: adminTwo.username}});
        await User.destroy({where: {username: premiumThree.username}});

    });
});