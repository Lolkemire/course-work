import * as supertest from 'supertest';
import {app} from '../../../src/app';
import {configs} from '../../../src/config/index';
import {DefaultRolesIds} from '../../../src/config/app';
import {Container} from 'typedi';
import User from '../../../src/database/entities/User';
import {Article} from '../../../src/database/entities/Article';
import {createUser} from '../../utills/createUser';
import {ArticlesService} from '../../../src/services/articles';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {Comment} from '../../../src/database/entities/Comment';



const request = supertest(app);
const appConfig = configs.getAppConfig();
const articlesService = Container.get(ArticlesService);

describe('Comments api tests', () => {
    const userOne: any = {
        username: 'updcom99UserOne',
        email: 'updcom99UserOne@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.USER
    };
    const adminTwo: any = {
        username: 'updcomUserTwo',
        email: 'updcomUserTwo@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN
    };
    const premiumThree: any = {
        username: 'updcomUserThree',
        email: 'updcomUserThree@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.PREMIUM
    };
    let instanceOfUserOne: User = null;
    let instanceOfAdminTwo: User = null;
    let instanceOfPremiumThree: User = null;

    let articleOne: Article = null;
    let articleTwo: Article = null;

    let deleteUserCommentOne: Comment = null;
    let deleteUserCommentTwo: Comment = null;
    let deletePremiumUserCommentOne: Comment = null;


    test('User with role "user" SHOULD delete own comment', async () => {
        const path = `/api/v1/comments/${deleteUserCommentOne.id}`;
        const response = await authorizeRequest(
            request.delete(path), userOne);
        expect(response.status).toBe(200);
    });

    test('User with role "user" SHOULDN\'T delete another comment', async () => {
        const path = `/api/v1/comments/${deletePremiumUserCommentOne.id}`;
        const response = await authorizeRequest(
            request.delete(path), userOne);
        expect(response.status).toBe(403);
    });

    test('User with role "premium user" SHOULD delete own comment', async () => {
        const path = `/api/v1/comments/${deletePremiumUserCommentOne.id}`;
        const response = await authorizeRequest(
            request.delete(path), premiumThree);
        expect(response.status).toBe(200);
    });

    test('User with role "premium user" SHOULDN\'T delete another comment', async () => {
        const path = `/api/v1/comments/${deleteUserCommentTwo.id}`;
        const response = await authorizeRequest(
            request.delete(path), premiumThree);
        expect(response.status).toBe(403);
    });

    test('User with role "admin" SHOULD delete comment', async () => {
        const path = `/api/v1/comments/${deleteUserCommentTwo.id}`;
        const response = await authorizeRequest(
            request.delete(path), adminTwo);
        expect(response.status).toBe(200);
    });



    beforeAll(async () => {
        instanceOfUserOne = await createUser(userOne) as User;
        instanceOfAdminTwo = await createUser(adminTwo) as User;
        instanceOfPremiumThree = await createUser(premiumThree) as User;
        articleOne = await Article.create({
            title: 'ArticleOne',
            text: 'Kek mek',
            author_id: instanceOfUserOne.id,
            image: ''
        }) as Article;
        articleTwo = await Article.create({
            title: 'ArticleOne',
            text: 'Kek mek',
            author_id: instanceOfPremiumThree.id,
            image: ''
        }) as Article;
        deleteUserCommentOne = await Comment.create({
            text: 'kek',
            author_id: instanceOfUserOne.id,
            article_id: articleOne.id,
        }) as Comment;
        deleteUserCommentTwo = await Comment.create({
            text: 'kek',
            author_id: instanceOfUserOne.id,
            article_id: articleOne.id,
        }) as Comment;
        deletePremiumUserCommentOne = await Comment.create({
            text: 'kek',
            author_id: instanceOfPremiumThree.id,
            article_id: articleOne.id,
        }) as Comment;
    });

    afterAll(async () => {
        const comments = await Comment.all();
        for (const comment of comments) {
            await comment.destroy();
        }
        await articlesService.deleteArticle(articleOne);
        await articlesService.deleteArticle(articleTwo);
        await User.destroy({where: {username: userOne.username}});
        await User.destroy({where: {username: adminTwo.username}});
        await User.destroy({where: {username: premiumThree.username}});

    });
});