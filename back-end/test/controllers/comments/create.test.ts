import * as supertest from 'supertest';
import {app} from '../../../src/app';
import {configs} from '../../../src/config/index';
import {DefaultRolesIds} from '../../../src/config/app';
import {Container} from 'typedi';
import User from '../../../src/database/entities/User';
import {Article} from '../../../src/database/entities/Article';
import {createUser} from '../../utills/createUser';
import {ArticlesService} from '../../../src/services/articles';
import {authorizeRequest} from '../../utills/authorizeRequest';
import {Comment} from '../../../src/database/entities/Comment';




const request = supertest(app);
const appConfig = configs.getAppConfig();
const articlesService = Container.get(ArticlesService);

describe('Create comments api tests', () => {
    const userOne: any = {
        username: 'createcom99UserOne',
        email: 'createcom99UserOne@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.USER
    };
    const adminTwo: any = {
        username: 'createcomUserTwo',
        email: 'createcomUserTwo@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN
    };
    const premiumThree: any = {
        username: 'createcomUserThree',
        email: 'createcomUserThree@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.PREMIUM
    };
    let instanceOfUserOne: User = null;
    let instanceOfAdminTwo: User = null;
    let instanceOfPremiumThree: User = null;

    let articleOne: Article = null;
    let articleTwo: Article = null;

    test('User with role "user" SHOULD create comment in own article', async () => {
        const commentData = {
            text: 'megaText',
            article_id: articleOne.id,
        };
        const path = `/api/v1/comments`;
        const response = await authorizeRequest(
            request.post(path).send(commentData), userOne);
        expect(response.status).toBe(201);
        expect(response.body['text']).toBe(commentData.text);
        expect(response.body['author_id']).toBe(instanceOfUserOne.id);
        expect(response.body['article_id']).toBe(articleOne.id);
    });

    test('User with role "user" SHOULDN\'T create comment in another article', async () => {
        const commentData = {
            text: 'megaText',
            article_id: articleTwo.id,
        };
        const path = `/api/v1/comments`;
        const response = await authorizeRequest(
            request.post(path).send(commentData), userOne);
        expect(response.status).toBe(403);
    });

    test('User with role "premium user" SHOULD create comment in own article', async () => {
        const commentData = {
            text: 'megaText',
            article_id: articleTwo.id,
        };
        const path = `/api/v1/comments`;
        const response = await authorizeRequest(
            request.post(path).send(commentData), premiumThree);
        expect(response.status).toBe(201);
        expect(response.body['text']).toBe(commentData.text);
        expect(response.body['author_id']).toBe(instanceOfPremiumThree.id);
        expect(response.body['article_id']).toBe(articleTwo.id);
    });

    test('User with role "premium user" SHOULD create comment in another article', async () => {
        const commentData = {
            text: 'megaText',
            article_id: articleOne.id,
        };
        const path = `/api/v1/comments`;
        const response = await authorizeRequest(
            request.post(path).send(commentData), premiumThree);
        expect(response.status).toBe(201);
        expect(response.body['text']).toBe(commentData.text);
        expect(response.body['author_id']).toBe(instanceOfPremiumThree.id);
        expect(response.body['article_id']).toBe(articleOne.id);
    });

    test('User with role "admin" SHOULD create comment in another article', async () => {
        const commentData = {
            text: 'megaText',
            article_id: articleTwo.id,
        };
        const path = `/api/v1/comments`;
        const response = await authorizeRequest(
            request.post(path).send(commentData), adminTwo);
        expect(response.status).toBe(201);
        expect(response.body['text']).toBe(commentData.text);
        expect(response.body['author_id']).toBe(instanceOfAdminTwo.id);
        expect(response.body['article_id']).toBe(articleTwo.id);
    });

    beforeAll(async () => {
        instanceOfUserOne = await createUser(userOne) as User;
        instanceOfAdminTwo = await createUser(adminTwo) as User;
        instanceOfPremiumThree = await createUser(premiumThree) as User;
        articleOne = await Article.create({
            title: 'ArticleOne',
            text: 'Kek mek',
            author_id: instanceOfUserOne.id,
            image: ''
        }) as Article;
        articleTwo = await Article.create({
            title: 'ArticleOne',
            text: 'Kek mek',
            author_id: instanceOfPremiumThree.id,
            image: ''
        }) as Article;
    });

    afterAll(async () => {
        await clear();
    });

    async function clear() {
        const comments = await Comment.all();
        for (const comment of comments) {
            await comment.destroy();
        }
        await articlesService.deleteArticle(articleOne);
        await articlesService.deleteArticle(articleTwo);
        await User.destroy({where: {username: userOne.username}});
        await User.destroy({where: {username: adminTwo.username}});
        await User.destroy({where: {username: premiumThree.username}});
    }
});