import {authorizeRequest} from '../utills/authorizeRequest';
import * as supertest from 'supertest';
import {app} from '../../src/app';
import {DefaultRolesIds} from '../../src/config/app';
import User from '../../src/database/entities/User';
import {createUser} from '../utills/createUser';
import * as fs from 'fs';
import * as path from 'path';
import {Article} from '../../src/database/entities/Article';
import {ArticlesService} from '../../src/services/articles';
import {Container} from 'typedi';
import {configs} from '../../src/config/index';


const request = supertest(app);
const appConfig = configs.getAppConfig();
const articlesService = Container.get(ArticlesService);


describe('Articles api tests', () => {
    const userOne: any = {
        username: 'artUserOne999',
        email: 'artUserOne999@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.USER
    };
    const adminTwo: any = {
        username: 'articlesUserTwo',
        email: 'articlesUserTwo@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.ADMIN
    };
    const premiumThree: any = {
        username: 'articlesUserThree',
        email: 'articlesUserThree@test.com',
        password: 'testtest',
        role_id: DefaultRolesIds.PREMIUM
    };
    let instanceOfUserOne: User = null;
    let instanceOfAdminTwo: User = null;
    let instanceOfPremiumThree: User = null;
    const createdArticles = [];
    const pathToFile = path.join(__dirname, '../', 'doge.jpg');


    describe('Article creation tests', () => {
        test('User with "user" role SHOULD create article', async () => {
            const newArticle = {
                title: 'Brand new Article',
                text: 'Mega test for new article',
            };
            const response = await authorizeRequest(
                request.post('/api/v1/articles')
                    .field('title', newArticle.title)
                    .field('text', newArticle.text)
                    .attach('image', pathToFile), userOne);
            expect(response.status).toBe(201);
            expect(response.body['title']).toBe(newArticle.title);
            expect(response.body['text']).toBe(newArticle.text);
            expect(response.body['author_id']).toBe(instanceOfUserOne.id);
            expect(response.body['author']['username']).toBe(instanceOfUserOne.username);
            expect(response.body['image']).not.toBeNull();
            createdArticles.push(response.body);
        });

        test('User with "premium user" role SHOULD create article', async () => {
            const newArticle = {
                title: 'Brand new Article',
                text: 'Mega test for new article',
                author_id: instanceOfPremiumThree.id,
            };
            const response = await authorizeRequest(
                request.post('/api/v1/articles')
                    .field('title', newArticle.title)
                    .field('text', newArticle.text)
                    .field('author_id', newArticle.author_id.toString())
                    .attach('image', pathToFile), premiumThree);
            expect(response.status).toBe(201);
            expect(response.body['title']).toBe(newArticle.title);
            expect(response.body['text']).toBe(newArticle.text);
            expect(response.body['author_id']).toBe(instanceOfPremiumThree.id);
            expect(response.body['author']['username']).toBe(instanceOfPremiumThree.username);
            expect(response.body['image']).not.toBeNull();
            createdArticles.push(response.body);
        });

        test('User with "admin" role SHOULDN\'T create article', async () => {
            const newArticle = {
                title: 'Brand new Article',
                text: 'Mega test for new article',
                author_id: instanceOfAdminTwo.id,
            };
            const response = await authorizeRequest(
                request.post('/api/v1/articles')
                    .field('title', newArticle.title)
                    .field('text', newArticle.text)
                    .field('author_id', newArticle.author_id.toString())
                    .attach('image', pathToFile), adminTwo);
            expect(response.status).toBe(403);
        });
    });

    describe('Article update tests', () => {
        let articleOne: Article = null;
        let articleTwo: Article = null;

        test('User with "user" role SHOULD update own article', async () => {
            const pathToArticle = `/api/v1/articles/${articleOne.id}`;
            const response = await authorizeRequest(request.patch(pathToArticle).send({
                title: 'testTitle',
                text: 'newText'
            }), userOne);
            expect(response.status).toBe(200);
            expect(response.body['title']).toBe('testTitle');
            expect(response.body['text']).toBe('newText');
        });

        test('User with "user" role SHOULD update own article and change image', async () => {
            const pathToArticle = `/api/v1/articles/${articleOne.id}`;
            const response = await authorizeRequest(request.patch(pathToArticle)
                .field('title', 'testTitle')
                .field('text', 'newText')
                .attach('image', pathToFile), userOne);
            expect(response.status).toBe(200);
            expect(response.body['title']).toBe('testTitle');
            expect(response.body['text']).toBe('newText');
            expect(response.body['image']).not.toBeNull();
            expect(response.body['image']).toContain('doge');
        });

        test('User with "premium user" role SHOULD update own article', async () => {
            const pathToArticle = `/api/v1/articles/${articleTwo.id}`;
            const response = await authorizeRequest(request.patch(pathToArticle).send({
                title: 'testTitle',
                text: 'newText'
            }), premiumThree);
            expect(response.status).toBe(200);
            expect(response.body['title']).toBe('testTitle');
            expect(response.body['text']).toBe('newText');
        });

        test('User with "user" role SHOULDN\'T update another article', async () => {
            const pathToArticle = `/api/v1/articles/${articleTwo.id}`;
            const response = await authorizeRequest(request.patch(pathToArticle).send({
                title: 'testTitle',
                text: 'newText'
            }), userOne);
            expect(response.status).toBe(403);
        });

        test('User with "premium user" role SHOULDN\'T update another article', async () => {
            const pathToArticle = `/api/v1/articles/${articleOne.id}`;
            const response = await authorizeRequest(request.patch(pathToArticle).send({
                title: 'testTitle',
                text: 'newText'
            }), premiumThree);
            expect(response.status).toBe(403);
        });

        test('User with "admin" role SHOULD update another article', async () => {
            const pathToArticle = `/api/v1/articles/${articleOne.id}`;
            const response = await authorizeRequest(request.patch(pathToArticle).send({
                title: 'testTitle',
                text: 'newText'
            }), adminTwo);
            expect(response.status).toBe(200);
            expect(response.body['title']).toBe('testTitle');
            expect(response.body['text']).toBe('newText');
        });

        beforeAll(async () => {
            articleOne = await Article.create({
                text: 'kek',
                title: 'kuk',
                author_id: instanceOfUserOne.id,
                image: ''
            }) as Article;
            articleTwo = await Article.create({
                text: 'kek',
                title: 'kuk',
                author_id: instanceOfPremiumThree.id,
                image: ''
            }) as Article;
        });

        afterAll(async () => {
            await articlesService.deleteArticle(articleOne);
            await articlesService.deleteArticle(articleTwo);
        });
    });

    describe('Article delete tests', () => {
        let articleOne: Article = null;


        test('User with "admin" role SHOULD delete article', async () => {
            const pathToArticle = `/api/v1/articles/${articleOne.id}`;
            const pathToImage = path.join(appConfig.basePath, articleOne.image);
            const response = await authorizeRequest(request.delete(pathToArticle), adminTwo);
            expect(response.status).toBe(200);
            // expect(fs.existsSync(pathToImage)).toBe(false);
        });

        beforeAll(async () => {
            articleOne = await Article.create({
                text: 'kek',
                title: 'kuk',
                author_id: instanceOfUserOne.id,
                image: ''
            }) as Article;
        });

        afterAll(async () => {
            await Article.destroy({where: {id: articleOne.id}});
        });
    });

    beforeAll(async () => {
        instanceOfUserOne = await createUser(userOne) as User;
        instanceOfAdminTwo = await createUser(adminTwo) as User;
        instanceOfPremiumThree = await createUser(premiumThree) as User;
    });

    afterAll(async () => {
        for (const article of createdArticles) {
            await articlesService.deleteArticle(await Article.findById(article.id) as Article);
        }

        await User.destroy({where: {username: userOne.username}});
        await User.destroy({where: {username: adminTwo.username}});
        await User.destroy({where: {username: premiumThree.username}});
    });
});