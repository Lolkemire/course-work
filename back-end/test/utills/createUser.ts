import {Container} from 'typedi';
import User from '../../src/database/entities/User';
import CreateUser from '../../src/models/users/createUser';
import {UserService} from '../../src/services/user';

const userService = Container.get(UserService);

export const createUser = async (data: any): Promise<User> => {
    const userData: CreateUser = new CreateUser();
    if (data instanceof CreateUser) {
        return await userService.createUser(data);
    } else {
        for (const key of Object.keys(data)) {
            userData[key] = data[key];
        }
    }
    return await userService.createUser(userData);
};
