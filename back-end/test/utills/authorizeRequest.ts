import * as supertest from 'supertest';
import { Container } from 'typedi';
import { AuthenticationService } from '../../src/services/authentication';

const authenticationService = Container.get(AuthenticationService);
export async function authorizeRequest(request: supertest.Test, user: any) {
    const result = await authenticationService
    .loginByEmail({
        email: user.email,
        password: user.password,
    });
    const token = 'Bearer ' + result['token'];
    return request.set('Authorization', token);
}
