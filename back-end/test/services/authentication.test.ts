import '../../src/database';
import {AuthenticationService} from '../../src/services/authentication';
import {Container} from 'typedi';
import CreateUser from '../../src/models/users/createUser';
import {createUser} from '../utills/createUser';
import User from '../../src/database/entities/User';
import LoginCredentials from '../../src/models/authentication/loginCredentials';
import {InvalidCredentialsError} from '../../src/errors/InvalidCredentialsError';
import RegistrationCredentials from '../../src/models/authentication/registrationCredentials';

const authenticationService = Container.get(AuthenticationService);

describe('Authentication service tests', () => {
    describe('Login tests', () => {
        const userToLogin: CreateUser = new CreateUser();
        userToLogin.username = 'userToLogin';
        userToLogin.email = 'userToLogin@test.com';
        userToLogin.password = 'testtest';

        test('it SHOULD create jwt token and plain user data', async () => {
            const loginCredentials = new LoginCredentials();
            loginCredentials.email = userToLogin.email;
            loginCredentials.password = userToLogin.password;
            const result = await authenticationService.loginByEmail(loginCredentials);
            expect(result).toHaveProperty('token');
            expect(result).toHaveProperty('user');
            expect(result.token).not.toBeNull();
            expect(result.user.username).toBe(userToLogin.username);
            expect(result.user.email).toBe(userToLogin.email);
            expect(result.token).toBe(authenticationService.generateJWTToken(result.user));
        });

        test('it SHOULD throw invalid credential error', async () => {
            const loginCredentials = new LoginCredentials();
            loginCredentials.email = userToLogin.email;
            loginCredentials.password = 'invalidPassword';
            try {
                const result = await authenticationService.loginByEmail(loginCredentials);
            } catch (e) {
                expect(e.name).toBe('InvalidCredentialsError');
                expect(e.message.detail).toBe('Invalid email or password!');
            }
        });

        beforeAll(async () => {
            await createUser(userToLogin);
        });

        afterAll(async () => {
            await User.destroy({where: {username: userToLogin.username}});
        });
    });

    describe('Registration tests', () => {
        const userToRegister: CreateUser = new CreateUser();
        userToRegister.username = 'userToLogin';
        userToRegister.email = 'userToLogin@test.com';
        userToRegister.password = 'testtest';

        test('it SHOULD register user', async () => {
            const registrationCredentials = new RegistrationCredentials();
            registrationCredentials.username = userToRegister.username;
            registrationCredentials.email = userToRegister.email;
            registrationCredentials.password = userToRegister.password;
            registrationCredentials.confirmPassword = userToRegister.password;
            const result = await authenticationService.registrationByEmail(registrationCredentials);
            expect(result).toHaveProperty('token');
            expect(result).toHaveProperty('user');
            expect(result.token).not.toBeNull();
            expect(result.user.username).toBe(userToRegister.username);
            expect(result.user.email).toBe(userToRegister.email);
            expect(result.token).toBe(authenticationService.generateJWTToken(result.user));
        });

        test('it SHOULD throw validation error because username not unique', async () => {
            const registrationCredentials = new RegistrationCredentials();
            registrationCredentials.username = userToRegister.username;
            registrationCredentials.email = userToRegister.email;
            registrationCredentials.password = userToRegister.password;
            registrationCredentials.confirmPassword = userToRegister.password;
            try {
                const result = await authenticationService.registrationByEmail(registrationCredentials);
            } catch (e) {
                expect(e.name).toBe('ValidationError');
                expect(e.message.detail).toBe('unique violation');
                expect(e.message.errors).toHaveProperty('username');
            }
        });

        test('it SHOULD throw validation error because email not unique', async () => {
            const registrationCredentials = new RegistrationCredentials();
            registrationCredentials.username = 'randomUniqueUsername9000';
            registrationCredentials.email = userToRegister.email;
            registrationCredentials.password = userToRegister.password;
            registrationCredentials.confirmPassword = userToRegister.password;
            try {
                const result = await authenticationService.registrationByEmail(registrationCredentials);
            } catch (e) {
                expect(e.name).toBe('ValidationError');
                expect(e.message.detail).toBe('unique violation');
                expect(e.message.errors).toHaveProperty('email');
            }
        });

        afterAll(async () => {
            await User.destroy({where: {username: userToRegister.username}});
        });
    });
});