FROM node:8.2.0

RUN mkdir /course-work
WORKDIR /course-work

COPY . /course-work

RUN cd front-end && npm install && npm run build:production
RUN cd back-end && npm install && npm run create-media && npm run build
EXPOSE 3000

CMD cd back-end && npm run start:production
